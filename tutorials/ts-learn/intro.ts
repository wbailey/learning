// following along here https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html

// in js
// const user = {
//   name: "Hayes",
//   id: 0,
// };

// in ts
interface User {
	name: string,
	id: number
}

interface User {
	bananaCount: number,
	addBanana: Function
}

// const user: User = {
// 	name: "Weston",
// 	id: 0,
// }

class UserAccount {
	name: string;
	id: number;
	bananaCount: number;
	constructor(name: string, id: number) {
		this.name = name;
		this.id = id;
		this.bananaCount = 0;
	}
	addBanana(): void {
		this.bananaCount++;
	}
}

// console.log(user)
const user: User = new UserAccount("Taco", 1)
user.addBanana()

console.log(user)



// functions that interact with users
// returns a user
function getAdminUser(): User {
	const adminUser: User = new UserAccount("chonk", 5)
	return adminUser
}

// takes a user as params
function deleteUser(user: User): void {
}

type myBool = true | false | "banana"

let myBoolInstance: myBool = "banana"

console.log(typeof myBoolInstance)

// make a string array
const myStringArray: [string] = ['banana']
// console.log(myStringArray)

// create an array that holds strings using generics:
const newArr: Array<string> = ['taco']
// console.log(newArr)

type StringArray = Array<string>;
type WithNameArray = Array<{ name: string }>

const cheeb: StringArray = ['hello']
console.log(cheeb)

// create an interfave with generics
interface Backpack<Type> {
	add: (obj: Type) => void;
	get: () => Type;
}

declare const backpack: Backpack<string>;
const object: string = backpack.get()

