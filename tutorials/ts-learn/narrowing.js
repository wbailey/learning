// https://www.typescriptlang.org/docs/handbook/2/narrowing.html
// function padLeft(padding: number | string, input: string): string {
// 	throw new Error("implemented yet!")
// }
// generic functions
function add(x, y) {
    if (typeof x == 'number' && typeof y == 'number') {
        return x + y;
    }
    else {
        throw new Error("banana");
    }
}
// console.log(add('1', '2'))
console.log(add(1, 2));
function typeCheck(type) {
    var valuesToCheck = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        valuesToCheck[_i - 1] = arguments[_i];
    }
    for (var i = 0; i < valuesToCheck.length; i++) {
        if (typeof type !== typeof valuesToCheck[i]) {
            return false;
        }
    }
    return true;
}
console.log(typeCheck("hello", 'a', 'b', 'c'));
console.log(typeCheck(1, 'hello', 2));
