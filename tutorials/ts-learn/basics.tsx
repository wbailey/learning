// https://www.typescriptlang.org/docs/handbook/2/basic-types.html
// https://github.com/Microsoft/TypeScript/wiki/TypeScript-Editor-Support#vim


// explicit types
function greet(person: string, date: Date) {
	console.log(`Hello, ${person}, today is ${date.toDateString()}`)
}

greet("The Monarch", new Date())

let myAny: any = "hello"
myAny = 19
console.log(myAny)

// object types
// function printCoord(pt: { x: number; y: number }) {
// 	console.log(`x: ${pt.x}, y: ${pt.y}`)
// }

printCoord({x: 20, y: 30});

// optional props
function printName(obj: {first: string, last?: string }) {
	console.log(obj)
	console.log(obj.last?.toUpperCase())
}

printName({ first: 'weston' });
printName({ first: 'weston', last: 'bailer' });

// https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#union-types
// unions
function printId(id: number | string) {
	console.log(id)
}

printId(10)
printId("hello")

type stringNum = string | number

let test: stringNum = 10

console.log(test)

printId(test)

// type Point = {
// 	x: number;
// 	y: number;
// }
// 
// function printCoord(pt: Point) {
// 	console.log(pt.x)
// 	console.log(pt.y)
// }
// 
// const point: Point = {
// 	x: 0xFF,
// 	y: 1.5 
// }
// 
// printCoord(point)

// interfaces can also create object types
interface Point {
	x: number;
	y: number;
}

function printCoord(pt: Point) {
	console.log(pt.x, pt.y);
}

const point: Point = {
	x: 45,
	y: 21
}

printCoord(point)
/*
Type aliases and interfaces are very similar, and in many cases you can choose between them freely. Almost all features of an interface are available in type, the key distinction is that a type cannot be re-opened to add new properties vs an interface which is always extendable.
 */
// const a: (string as any) as number = 'hello'
// console.log(a)

enum Direction {
	Up = 1,
	Down,
	Left, 
	Right
}

let dir: Direction = Direction.Up 

console.log(dir)
dir = Direction.Down 
console.log(dir)

// https://www.typescriptlang.org/docs/handbook/2/narrowing.html
function padLeft(padding: number | string, input: string): string {
	throw new Error("implemented yet!")
}


