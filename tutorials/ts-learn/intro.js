// following along here https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html
// const user: User = {
// 	name: "Weston",
// 	id: 0,
// }
var UserAccount = /** @class */ (function () {
    function UserAccount(name, id) {
        this.name = name;
        this.id = id;
        this.bananaCount = 0;
    }
    UserAccount.prototype.addBanana = function () {
        this.bananaCount++;
    };
    return UserAccount;
}());
// console.log(user)
var user = new UserAccount("Taco", 1);
user.addBanana();
console.log(user);
// functions that interact with users
// returns a user
function getAdminUser() {
    var adminUser = new UserAccount("chonk", 5);
    return adminUser;
}
// takes a user as params
function deleteUser(user) {
}
var myBoolInstance = "banana";
console.log(typeof myBoolInstance);
// make a string array
var myStringArray = ['banana'];
// console.log(myStringArray)
// create an array that holds strings using generics:
var newArr = ['taco'];
var cheeb = ['hello'];
console.log(cheeb);
var backpack;
var object = backpack.get();
