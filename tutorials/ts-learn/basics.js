// https://www.typescriptlang.org/docs/handbook/2/basic-types.html
// https://github.com/Microsoft/TypeScript/wiki/TypeScript-Editor-Support#vim
// explicit types
function greet(person, date) {
    console.log("Hello, ".concat(person, ", today is ").concat(date.toDateString()));
}
greet("The Monarch", new Date());
var myAny = "hello";
myAny = 19;
console.log(myAny);
// object types
// function printCoord(pt: { x: number; y: number }) {
// 	console.log(`x: ${pt.x}, y: ${pt.y}`)
// }
printCoord({ x: 20, y: 30 });
// optional props
function printName(obj) {
    var _a;
    console.log(obj);
    console.log((_a = obj.last) === null || _a === void 0 ? void 0 : _a.toUpperCase());
}
printName({ first: 'weston' });
printName({ first: 'weston', last: 'bailer' });
// https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#union-types
// unions
function printId(id) {
    console.log(id);
}
printId(10);
printId("hello");
var test = 10;
console.log(test);
printId(test);
function printCoord(pt) {
    console.log(pt.x, pt.y);
}
var point = {
    x: 45,
    y: 21
};
printCoord(point);
/*
Type aliases and interfaces are very similar, and in many cases you can choose between them freely. Almost all features of an interface are available in type, the key distinction is that a type cannot be re-opened to add new properties vs an interface which is always extendable.
 */
// const a: (string as any) as number = 'hello'
// console.log(a)
var Direction;
(function (Direction) {
    Direction[Direction["Up"] = 1] = "Up";
    Direction[Direction["Down"] = 2] = "Down";
    Direction[Direction["Left"] = 3] = "Left";
    Direction[Direction["Right"] = 4] = "Right";
})(Direction || (Direction = {}));
var dir = Direction.Up;
console.log(dir);
dir = Direction.Down;
console.log(dir);
// https://www.typescriptlang.org/docs/handbook/2/narrowing.html
function padLeft(padding, input) {
    throw new Error("implemented yet!");
}
