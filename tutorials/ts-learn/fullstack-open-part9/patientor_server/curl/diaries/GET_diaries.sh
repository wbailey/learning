#!/bin/bash
. curl/config.sh

URL_PATH="/api/diaries"

curl "${BASE_URL}${URL_PATH}" \
	--include \
	--request GET

echo
