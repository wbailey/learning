#!/bin/bash
. curl/config.sh

URL_PATH="/api/diaries"

curl "${BASE_URL}${URL_PATH}" \
	--include \
	--request POST \
	--header "Content-Type: application/json" \
	--data '{
		"ping": "'"${URL_PATH}"'"
	}'

echo
