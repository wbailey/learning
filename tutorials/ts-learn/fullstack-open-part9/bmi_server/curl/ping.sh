#!/bin/bash

API="http://127.0.0.1"
PORT="3003"
URL_PATH="/ping"

URL="${API}:${PORT}${URL_PATH}"

curl $URL \
	--include \
	--request GET

echo

