#include <stdio.h>
#include "funcs.h"

int main(void)
{
	int num_one = add(10, 11);
	int num_two = sub(3, 2);
	printf("num_one is: %d, num_two is %d\n", num_one, num_two);
	return 0;
}
