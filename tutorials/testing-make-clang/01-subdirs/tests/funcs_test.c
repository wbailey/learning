#include <criterion/criterion.h>
#include <stdio.h>
#include "../src/funcs.h"

int out_num = 10;

void suitesetup(void)
{
	out_num++;
	printf("setting up...\n");
}

void suiteteardown(void)
{
	printf("tearingdown...\n");
}

TestSuite(funcs_test, .init=suitesetup, .fini=suiteteardown);

Test(funcs_tests, add) {
	int num = add(10, 15);
	cr_expect(num == 25, "num should equal 25");
	cr_expect(out_num == 10, "out_num should equal 11 and it is %d", out_num);
}

Test(funcs_tests, sub) {
	int num = sub(10, 15);
	cr_expect(num == -5, "num should equal -5 and num is %d", num);
}

