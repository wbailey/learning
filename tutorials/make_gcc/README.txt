https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html

# preprocess the file
cpp hello.c > hello.i
# create an linkable object file
gcc -S hello.i
# assemble the object file
as -o hello.o hello.s
# compile to binary
# ld -o hello.exe hello.o
gcc -v -o hello.exe hello.o
# run
./hello.exe

# nm is used to see the functions ins an object file
nm hello.o
# ldd shows the required libraries that need to be linked to an executable
ldd hello.exe

