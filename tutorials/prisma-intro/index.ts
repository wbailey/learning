import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
	// create new user
	await prisma.user.create({
		data: {
			name: "Weston",
			email: "wb@email.com" + String(Math.random()),
			posts: {
				create: { 
					title: "Hello World"
				}
			},
			profile: {
				create: {
					bio: "I like turtles"
				}
			}
		}
	});
	// find all users
	const allUsers = await prisma.user.findMany({
		include: {
			posts: true,
			profile: true
		}
	})
	console.dir(allUsers, { depth: null });
}


main()
	.then(async () => await prisma.$disconnect())
	.catch(async e => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});

