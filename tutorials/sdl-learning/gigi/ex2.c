// https://gigi.nullneuron.net/gigilabs/displaying-an-image-in-an-sdl2-window/
#include <SDL2/SDL.h>
#include <stdbool.h>

#define WIN_WIDTH 1280
#define WIN_HEIGHT 960

int main (int argc, char *argv[]) 
{
	bool quit = false;
	SDL_Event event;
	
	SDL_Init (SDL_INIT_VIDEO);

	SDL_Window *window = SDL_CreateWindow("SDL2 Displaying an Image", SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, WIN_WIDTH, WIN_HEIGHT, 0);

	// renderer represents the output device (graphics card)
	// SDL_CreateRenderer (window to draw on, int to select driver (-1 = default), SDL_RendererFlags (0 defaults to rendering))
	SDL_Renderer *renderer = SDL_CreateRenderer (window, -1, 0);
	// takes a path to a bitmap and mounts it on a SDL surface (cpu mem)
	SDL_Surface *image = SDL_LoadBMP ("./chonker.bmp");
	// turn the image into a texture, a texture is memory close to the graphics card (gpu mem)
	SDL_Texture *texture = SDL_CreateTextureFromSurface (renderer, image);
	
	double x  = 0;
	double y = 0;
	double direction_x = .01;
	double direction_y = .01;

	while (!quit) {
		/* SDL_WaitEvent (&event); */
		// poll all events until the queue is empty
		while (SDL_PollEvent (&event)) {
			switch (event.type) {
				case SDL_QUIT: 
					quit = true;
					break;
				case SDL_KEYDOWN:
					printf ("keydown detected: %s, code: %s\n", 
							SDL_GetKeyName (event.key.keysym.sym), 
							SDL_GetScancodeName(event.key.keysym.scancode));
					break;
				case SDL_KEYUP:
					printf ("keyup detected: %s\n", SDL_GetKeyName (event.key.keysym.sym));
					break;
			}
		}
		// move image
		x += direction_x;
		y += direction_y;
		if (x >= WIN_WIDTH - 640 || x <= 0) {
			direction_x *= -1;
		}
		if (y >= WIN_HEIGHT - 480 || y <=0) {
			direction_y *= -1;
		}
		// set color to be black
		SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
		// clear the rect
		SDL_RenderClear(renderer);
		// display image on the window
		// SDL_Rect defines a square starting in upper left corner
		// Designated initialisers
		SDL_Rect dstrect = { .x = x, .y = y, .w = 640, .h = 480 };
		// SDL_RenderCopy copies the texture to the output device
		SDL_RenderCopy(renderer, texture, NULL, &dstrect);
		// commits the texture to video memory, and displays it
		SDL_RenderPresent (renderer);

	}

	SDL_Quit();

	return 0;
}
