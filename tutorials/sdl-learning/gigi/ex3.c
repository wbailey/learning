// https://gigi.nullneuron.net/gigilabs/loading-images-in-sdl2-with-sdl_image/
#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <stdbool.h>

int main (int argc, char *argv[])
{
	bool quit = false;
	SDL_Event event;

	SDL_Init(SDL_INIT_VIDEO);
	// init image right after SDL
	IMG_Init (IMG_INIT_JPG);

	SDL_Window *window = SDL_CreateWindow("SDL2 Displaying Image",
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, 0);

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
	/* SDL_Surface *image = SDL_LoadBMP("chonker.bmp"); */
	SDL_Surface *image = IMG_Load ("window-chonker.jpg");
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, image);

	while (!quit) {
		SDL_WaitEvent(&event);

		switch (event.type) {
			case SDL_QUIT:
				quit = true;
				break;
		}


		// SDL_Rect dstrect = { .x = 5, .y = 5, .w = 320, .h = 240 };
		// SDL_RenderCopy (renderer, texture, NULL, &dstrect);
		SDL_RenderCopy (renderer, texture, NULL, NULL);
		SDL_RenderPresent (renderer);
	}

	SDL_DestroyTexture (texture);
	SDL_FreeSurface (image);
	SDL_DestroyRenderer (renderer);
	SDL_DestroyWindow (window);

	SDL_Quit();

	return 0;
}
