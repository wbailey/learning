// https://gigi.nullneuron.net/gigilabs/showing-an-empty-window-in-sdl2/
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>

int main (int argc, char *argv[])
{	
	bool quit = false;
	SDL_Event event;
	// tell SDL what subsystems we need
	SDL_Init (SDL_INIT_VIDEO);
	// 
	SDL_Window *screen = SDL_CreateWindow("My SDL EMPTY WINDOW", 
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, 0);
	// delay window closing 
	// SDL_Delay(3000);

	// keep the window open until there is a keypress event
	while (!quit) {
		// waits indefinately for the next event
		SDL_WaitEvent (&event);
		// would also be possible to use SDL_PollEvent, which uses more CPU
		// SDL_PollEvent(&event);
		// puts("Event Polled");
		switch (event.type) {
			case SDL_QUIT:
				quit = true;
				break;
		}
	}

	// cleans up and closes SDL
	SDL_Quit();

	puts("Exit Success");

	return 0;
}
