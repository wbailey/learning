const canvas = document.querySelector('canvas')
canvas.setAttribute("height", getComputedStyle(canvas)["height"])
canvas.setAttribute("width", getComputedStyle(canvas)["width"])
const ctx = canvas.getContext('2d')


ctx.fillStyle = 'red'
ctx.fillRect(0, 0, 10, 10)
const tileSize = 10
const spriteSheet = new Image(330, 370)
spriteSheet.src = './sprites/Full.png'
spriteSheet.onload = () => {
  console.dir(spriteSheet)
  ctx.drawImage(spriteSheet, 0, 0, 330, 370)
}
