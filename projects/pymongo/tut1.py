from pymongo import MongoClient

# connect to your mongo
client = MongoClient('mongodb://localhost:27017/')

# name the database (can use dot notation as well)
db = client['test-database']

# create a collection
posts = db.posts

# https://pymongo.readthedocs.io/en/stable/api/pymongo/collection.html#pymongo.collection.Collection.insert_many

# post = {
#   'author': 'Mike',
#   'text': 'awesome post',
#   'tags': ["mongodb", "python", "pymongo"],
# } 

# # insert new post
# insterted_post = posts.insert_one(post)
# print('inserted post:', insterted_post)
# print('post id:', insterted_post.inserted_id)

print(db.list_collection_names())

found_post = posts.find_one({
  "author": "Mike"
})

# https://pymongo.readthedocs.io/en/stable/api/pymongo/collection.html#pymongo.collection.Collection.insert_many

def find_by_id(id: str, collection: str) -> dict:
  from bson.objectid import ObjectId
  return db[collection].find_one({ "_id": ObjectId(id) })

find_id = find_by_id('60eb89061ad2dd6c0ad98f38', 'posts')

print(find_id)

print(found_post)