// gcc main.c  -Wall -g -I/usr/include/SDL2 -D_REENTRANT -lSDL2 -lSDL2_image -o main
#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stddef.h>

// http://gamedevgeek.com/tutorials/moving-sprites-with-sdl/
#define WIN_HEIGHT 960	
#define WIN_WIDTH 1280 

int main (int argc, char *argv[]) 
{
	bool quit = false;
	// handle input
	SDL_Event event;
	const Uint8 *keystate;
	// delta/fps
	Uint32 total_frame_ticks = 0;
	Uint32 total_frames = 0;
	float total_ms = 0.0f;
	// game vars
	int speed = 2;
	// fire up SDL
	SDL_Init (SDL_INIT_VIDEO);
	SDL_Window *window = SDL_CreateWindow ("Pong", SDL_WINDOWPOS_UNDEFINED, 
			SDL_WINDOWPOS_UNDEFINED, WIN_WIDTH, WIN_HEIGHT, 0);

	SDL_Renderer *renderer = SDL_CreateRenderer (window, -1, SDL_RENDERER_ACCELERATED);
	
	SDL_Rect rect = { .x = 30, .y = 50, .w = 200, .h = 300 };

	// gameloop
	while (!quit) {
		// start frame timing
		total_frames++;
		Uint32 start_ticks = SDL_GetTicks ();
		Uint32 start_perf = SDL_GetPerformanceCounter ();

		// handle input
		while (SDL_PollEvent (&event)) {
			switch (event.type) {
				// close button
				case SDL_QUIT:
					quit = true;
					break;
				// quit keys
				case SDL_KEYDOWN:
					// printf ("Keydown: %s, code: %s\n",
					// 		SDL_GetKeyName (event.key.keysym.sym),
					// 		SDL_GetScancodeName (event.key.keysym.scancode));
						switch (event.key.keysym.sym) {
							case SDLK_ESCAPE:
							case SDLK_q:
								quit = true;
								break;
						}
					
					break;
			// 	case SDL_KEYUP: 
			// 		printf ("Keyup: %s, code: %s\n",
			// 				SDL_GetKeyName (event.key.keysym.sym),
			// 				SDL_GetScancodeName (event.key.keysym.scancode));
			// 		break;
			}
		}

		// movement
		// https://wiki.libsdl.org/SDL2/SDL_Keycode
		keystate = SDL_GetKeyboardState(NULL);
		if (keystate[SDL_SCANCODE_W]) {
			rect.y -= speed;
		}
		if (keystate[SDL_SCANCODE_S]) {
			rect.y += speed;
		}
		if (keystate[SDL_SCANCODE_A]) {
			rect.x -= speed;
		}
		if (keystate[SDL_SCANCODE_D]) {
			rect.x += speed;
		}
		// gamelogic		
		// re-render
		// clear
		SDL_SetRenderDrawColor( renderer, 0x00, 0x00, 0x00, 0x00);
		SDL_RenderClear (renderer);
		// draw
		SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xFF, 0x00);
		SDL_RenderFillRect (renderer, &rect);
		SDL_RenderPresent (renderer);

		// end frame timing
		Uint32 end_ticks = SDL_GetTicks ();
		Uint64 end_perf = SDL_GetPerformanceCounter ();
		Uint64 frame_perf = end_perf - start_perf;
		total_frame_ticks += end_ticks - start_ticks;
		float frame_time = (end_ticks - start_ticks) / 1000.0f;
		float elapsed_ms = (end_perf - start_perf) / ((float) SDL_GetPerformanceFrequency() * 1000.0f); 

		total_ms += elapsed_ms;
		if (total_ms >= 5000.0f) {
			printf("============================\n");
			printf("Current FPS: %f\n", frame_time);
			printf("Average FPS: %f\n", 1000.0f / ((float) total_frame_ticks / total_frames));
			printf("Current perf: %f\n", (float) frame_perf);
			printf("Elapsed ms: %f\n", elapsed_ms);
			printf("Total ms: %f\n", total_ms);
			total_ms = 0;
		}
		// Cap to 60 FPS
		SDL_Delay((int)  (16.666f / elapsed_ms));
	}
	
	// quit sdl
	SDL_DestroyRenderer (renderer);
	SDL_DestroyWindow (window);
	SDL_Quit ();
	return 0;
}
