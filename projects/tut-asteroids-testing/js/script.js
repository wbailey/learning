/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ VARIABLES & CONSTANTS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
const TWO_PI = Math.PI * 2;
let canvas, ctx, ship;                      // canvas and context and ship
let canvasWidth = 1100;               //canvas dimensions
let canvasHeight = 800;
let keys = [];                         //array to store key presses
let bullets = [];
let asteroids = [];
let score = 0;
let lives = 3;

document.addEventListener(`DOMContentLoaded`, setupCanvas); //call setup canvas on page load

function setupCanvas() {
  canvas = document.getElementById(`my-canvas`);
  ctx = canvas.getContext(`2d`);
  canvas.width = canvasWidth;
  canvas.height = canvasHeight;
  ctx.fillStyle = `black`;
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  document.body.addEventListener(`keydown`, function(e){
    keys[e.keyCode] = true;
  });
  document.body.addEventListener(`keyup`, function(e){
    keys[e.keyCode] = false;
    if (e.keyCode === 32){
      bullets.push(new Bullet(ship.angle));
    }
  });
  ship = new Ship();                                          //init keys array to all off
  for (let i = 0; i < 8; i++){
    asteroids.push(new Asteroid());
  }
  for (let i = 0; i < 88; i++){
    keys[i] = false;
  }
  render();
}

class Ship {
  constructor(){
    this.visibile = true;                   //can you see the ship?
    this.x = canvasWidth / 2;               //starting x value in the middle of the screen
    this.y = canvasHeight / 2;
    this.moving = false;             //if ship is moving
    this.speed = 0.1;                       //speed of movement
    this.velX = 0;                          //velocity on x axis
    this.velY = 0;
    this.rotateSpeed = 0.001;               //speed ship rotates
    this.radius = 15;                       //size on ship (hypotenuese)
    this.angle = 0;                         //staring angle of 0
    this.strokeColor = `white`;             //actual ship color
    this.noseX = canvasWidth / 2 + 15;      //starting position of nose
    this.noseY = canvasHeight / 2;
  }
  rotate(dir){
    this.angle += this.rotateSpeed * dir;  //add speed to angle multiplied by direction (+/-1) to roatate
  }
  update(){                                 //moves ship
    let radians = this.angle / Math.PI * 180;   //convert current direction of ship to from degrees to radians
    //this.speed = this.speed * this.direction;
    if(this.moving){                     
      this.velX += Math.cos(radians) * this.speed; //oldX + cos(radians) * distance to calculate moving forward
      this.velY += Math.sin(radians) * this.speed; //oldy + sin(radians) * distance to calculate moving forward on y
    } 
    if(this.x < this.radius){                     //move ship to other side of screen when it reaches the side
      this.x = canvasWidth;
    }       
    if(this.x > canvasWidth){                     //move ship to other side of screen when it reaches the side
      this.x = this.radius;
    }       
    if(this.y < this.radius){                     //move ship to other side of screen when it reaches the side
      this.y = canvasHeight;
    }       
    if(this.y > canvasHeight){                     //move ship to other side of screen when it reaches the side
      this.y = this.radius;
    } 
    this.velX *= 0.99;                             //change velocity on update to sinmulate slowing down
    this.velY *= 0.99;   
    
    this.x -= this.velX;
    this.y -= this.velY;
  }
  draw(){
    ctx.strokeStyle = this.strokeColor;           //color defined when object created
    ctx.beginPath();
    let vertAngle = TWO_PI / 3;                   //vertice angle is 1/3 of 360 degrees (two pi) to make a 3 sided polygon
    let radians = (this.angle / Math.PI) * 180;    //convert degeres to radians
    this.noseX = this.x - this.radius * Math.cos(radians);  //recalculate position of nose
    this.noseY = this.y - this.radius * Math.sin(radians);
    for(let i = 0; i < 3; i++){
      ctx.lineTo(this.x - this.radius * Math.cos(vertAngle * i + radians), this.y - this.radius * Math.sin(vertAngle * i + radians));
    }
    ctx.closePath();
    ctx.stroke();
    //console.log(this.speed)
  }
}

class Bullet {
  constructor(angle) {
    this.visible = true;
    this.x = ship.noseX; //start from position of ship's nose
    this.y = ship.noseY; 
    this.angle = angle; //trajectory is passed when object is created
    this.height = 4;
    this.width = 4;
    this.speed = 5; //speed of bullet
    this.velX = 0;  //positon on update
    this.vely = 0;
    this.strokeColor = 'green';
  }
  update(){
    var radians = this.angle / Math.PI * 180;
    this.x -= Math.cos(radians) * this.speed;
    this.y -= Math.sin(radians) * this.speed;
    if(this.x < this.radius){                     
      this.x = canvasWidth;
    }       
    if(this.x > canvasWidth){                     
      this.x = this.radius;
    }       
    if(this.y < this.radius){                     
      this.y = canvasHeight;
    }        
    if(this.y > canvasHeight){                     
      this.y = this.radius;
    } 
  }
  draw(){
    ctx.fillStyle = this.strokeColor;
    ctx.fillRect(this.x, this.y, this.width, this.height);
  }
}

class Asteroid {
  constructor(x, y, radius, level, collisonRadius){ //passed random starting position
    this.visibile = true;
    this.x = x || Math.floor(Math.random() * canvasWidth); //create random starting position or use x
    this.y = y || Math.floor(Math.random() * canvasHeight);
    this.speed = .5;
    this.radius = radius || 50;
    this. angle = Math.floor(Math.random() * 359); //random angle at creation
    this.strokeColor = `white`;
    this. collisonRadius = 46 || collisonRadius;
    this.level = level || 1; //level is asteroid size
  }
  update(){
    var radians = this.angle / Math.PI * 360;
    this.x += Math.cos(radians) * this.speed;
    this.y += Math.sin(radians) * this.speed;
    if(this.x < this.radius){                     
      this.x = canvasWidth;
    }       
    if(this.x > canvasWidth){                     
      this.x = this.radius;
    }       
    if(this.y < this.radius){                     
      this.y = canvasHeight;
    }       
    if(this.y > canvasHeight){                     
      this.y = this.radius;
    } 
  }
  draw(){
    ctx.strokeStyle = this.strokeColor;
    ctx.beginPath();
    let vertAngle = ((Math.PI * 2) / 6);
    let radians = this.angle / Math.PI * 180;
    for(let i = 0; i < 6; i++){
      ctx.lineTo(this.x - this.radius * Math.cos(vertAngle * i + radians), this.y - this.radius * Math.sin(vertAngle * i + radians));
    }
    ctx.closePath();
    ctx.stroke();
  }
}

function circleCollision(p1x, p1y, r1, p2x, p2y, r2){
  //console.log(arguments)
  let radiusSum;
  let xDiff;
  let yDiff;
  radiusSum = r1 + r2
  xDiff = p1x - p2x;
  yDiff = p1y - p2y;
  if(radiusSum > Math.sqrt((xDiff * xDiff) + (yDiff* yDiff))){
    return true;
  } else {
    return false;
  }
}

function drawLifeShips(){
  let startX = 1100;
  let startY = 10;
  let points = [[9, 9], [-9, 9]];
  ctx.strokeStyle = ship.strokeStyle;
  for(let i = 0; i < lives; i++){
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    for(let j = 0; j < points.length; j++){
      ctx.lineTo(startX + points[j][0], startY + points[j][1]);
    }
    ctx.closePath();
    ctx.stroke();
    startX -= 30;
  }
}


function render(){
  if(keys[87]){
    ship.moving = keys[87];  //controls 87 = w 68 = D 65 = A S = 83 update speed to control direction
    ship.speed = .1;
  } else if(keys[83]){
    ship.moving = keys[83];  //controls 87 = w 68 = D 65 = A S = 83
    ship.speed = -.1;
  } else if (!keys[87] && !keys[83]){
    ship.moving = false;
    ship.speed = 0;
  }
  if(keys[68]){
    ship.rotate(1);
  }
  if(keys[65]){
    ship.rotate(-1);
  }
  ctx.clearRect(0, 0, canvasWidth, canvasHeight); //clear canvas
  ctx.fillStyle = ship.fillStyle;
  ctx.font = '21px Arial';
  ctx.fillText(`SCORE: ${score}`, 20, 35);
  if(lives <= 0){
    ship.visibile = false;
    ctx.fillStyle = ship.fillStyle;
    ctx.font = '50px Arial';
    ctx.fillText('GAME OVER', canvasWidth / 2 - 150, canvasHeight / 2);
  }
  drawLifeShips();
  if(asteroids.length != 0){
    for(let k = 0; k < asteroids.length; k++){
      if(circleCollision(ship.x, ship.y, 11, asteroids[k].x, asteroids[k].y, asteroids[k].radius)){
        // ship.x = canvasWidth / 2;
        // ship.y = canvasHeight / 2;w
        // ship.velX = 0;
        // ship.velY = 0;
        // lives -= 1;
        ship.strokeColor = 'red';
        asteroids[k].strokeColor = `green`
        console.log(`hit`)
      } else {
        ship.strokeColor = `white`;
        asteroids[k].strokeColor = `white`
      }
    }
  }
  if(asteroids.length !== 0 && bullets.length !== 0){
loop1:
    for(let l = 0; l < asteroids.length; l++){
      for(let m = 0; m < bullets.length; m++){
        if(circleCollision(bullets[m].x, bullets[m].y, 3, asteroids[l].x, asteroids[l].y, asteroids[l].collisonRadius)){
          if(asteroids[l].level === 1){
            asteroids.push(new Asteroid(asteroids[l].x - 5, asteroids[l].y - 5, 25, 2, 22));
            asteroids.push(new Asteroid(asteroids[l].x + 5, asteroids[l].y + 5, 25, 2, 22));
          } else if (asteroids[l].level === 2){
            asteroids.push(new Asteroid(asteroids[l].x - 5, asteroids[l].y - 5, 15, 3, 12));
            asteroids.push(new Asteroid(asteroids[l].x + 5, asteroids[l].y + 5, 15, 3, 12));
          }
          asteroids.splice(l, 1);
          bullets.splice(m, 1);
          score += 20;
          break loop1;
        }
      }
    }
  }
  if(ship.visibile){
    ship.update();                                  //update ship
    ship.draw();                                    //update draw
  }
  if(bullets.length !== 0){
    for(let i = 0; i < bullets.length; i++){
      bullets[i].update();
      bullets[i].draw();
    }
  }
  if(asteroids.length !== 0){
    for(let j = 0; j < asteroids.length; j++){
      asteroids[j].update();
      asteroids[j].draw(j);
    }
  }
  requestAnimationFrame(render);                  //calls render to animate
  //console.log(asteroids[0].collisonRadius)

}

