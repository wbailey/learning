/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/scripts/MainLoop.js":
/*!*********************************!*\
  !*** ./src/scripts/MainLoop.js ***!
  \*********************************/
/***/ ((module) => {

eval("/**\n * MainLoop accepts a callback function and passes in the callback as an argument\n * Mainloop(console.log) -- logs time elapsed from last render\n * @param {*} callback - a callback function to run every loop \n * @returns request animation frame's return\n */\nfunction MainLoop(callback, fps) {\n  // get timestamp when first starting \n  var startTime = new Date(); // this return function is IIFE to start the loop immediately\n\n  return function loop(prevTime) {\n    // only use starttime first\n    prevTime = prevTime ? prevTime : startTime; // calc th time elapses\n\n    var newTime = new Date();\n    var delta = newTime - prevTime; // run the main loop logic\n\n    callback(delta); // recurse this function\n\n    return requestAnimationFrame(function () {\n      loop(newTime);\n    });\n  }();\n}\n\nmodule.exports = MainLoop;\n\n//# sourceURL=webpack://webpack-html-template/./src/scripts/MainLoop.js?");

/***/ }),

/***/ "./src/scripts/index.js":
/*!******************************!*\
  !*** ./src/scripts/index.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("var canvas = document.querySelector('canvas');\ncanvas.setAttribute(\"height\", getComputedStyle(canvas)[\"width\"]);\ncanvas.setAttribute(\"width\", getComputedStyle(canvas)[\"width\"]);\nvar ctx = canvas.getContext('2d');\n\nvar MainLoop = __webpack_require__(/*! ./MainLoop.js */ \"./src/scripts/MainLoop.js\"); // const characterSpriteData = require('../sprites/MYSTIC/characters/characterIdle.json')\n// find the smaller side\n\n\nvar frame = window.innerHeight < window.innerWidth ? window.innerHeight : window.innerWidth;\ncanvas.setAttribute(\"height\", frame);\ncanvas.setAttribute(\"width\", frame); // game state\n\nvar state = {\n  canvas: canvas,\n  ctx: ctx,\n  unit: canvas.width * .05,\n  initialSpeed: canvas.width * .05 * 7\n};\n\nfunction Vector(_ref) {\n  var x = _ref.x,\n      y = _ref.y;\n\n  var update = function update(_ref2) {\n    var x = _ref2.x,\n        y = _ref2.y;\n    this.x = x;\n    this.y = y;\n  };\n\n  return {\n    x: x,\n    y: y,\n    update: update\n  };\n}\n\nfunction Crawler(_ref3) {\n  var vector = _ref3.vector,\n      width = _ref3.width,\n      height = _ref3.height,\n      color = _ref3.color,\n      state = _ref3.state;\n  var ctx = state.ctx;\n\n  var render = function render() {\n    var color = this.color,\n        width = this.width,\n        height = this.height,\n        vector = this.vector;\n    var x = vector.x,\n        y = vector.y;\n    ctx.fillStyle = color;\n    ctx.fillRect(x, y, width, height);\n  };\n\n  return {\n    vector: vector,\n    width: width,\n    height: height,\n    color: color,\n    render: render\n  };\n}\n\nfunction SpriteCrawler(_ref4) {\n  var state = _ref4.state,\n      vector = _ref4.vector,\n      imagePath = _ref4.imagePath,\n      width = _ref4.width,\n      height = _ref4.height;\n  var floor = Math.floor;\n  var ctx = state.ctx;\n  var image = new Image();\n  image.src = imagePath;\n\n  var render = function render() {\n    var image = this.image,\n        width = this.width,\n        height = this.height,\n        vector = this.vector;\n    var x = vector.x,\n        y = vector.y;\n    x = floor(x);\n    y = floor(y);\n    ctx.drawImage(image, 0, 0, 64, 64, x, y, 64, 64);\n  };\n\n  return {\n    vector: vector,\n    image: image,\n    width: width,\n    height: height,\n    render: render\n  };\n}\n\nvar crawler = Crawler({\n  vector: Vector({\n    x: 100,\n    y: 100\n  }),\n  width: state.unit,\n  height: state.unit,\n  color: 'green',\n  state: state\n});\nvar spriteCrawler = SpriteCrawler({\n  state: state,\n  imagePath: spritePath,\n  vector: Vector({\n    x: 20,\n    y: 20\n  }),\n  width: 64,\n  height: 64\n});\n\nfunction HandleInput(state) {\n  // speed per 1000 ms\n  var speed = state.initialSpeed;\n  console.log(speed);\n\n  var up = function up(player, deltaUpdate) {\n    player.vector.update({\n      x: player.vector.x,\n      y: player.vector.y - speed * deltaUpdate\n    });\n  };\n\n  var down = function down(player, deltaUpdate) {\n    player.vector.update({\n      x: player.vector.x,\n      y: player.vector.y + speed * deltaUpdate\n    });\n  };\n\n  var left = function left(player, deltaUpdate) {\n    player.vector.update({\n      x: player.vector.x - speed * deltaUpdate,\n      y: player.vector.y\n    });\n  };\n\n  var right = function right(player, deltaUpdate) {\n    player.vector.update({\n      x: player.vector.x + speed * deltaUpdate,\n      y: player.vector.y\n    });\n  };\n\n  var getSpeed = function getSpeed() {\n    return speed;\n  };\n\n  var setSpeed = function setSpeed(newSpeed) {\n    speed = newSpeed;\n  };\n\n  var handlers = [{\n    triggers: ['w'],\n    callback: up\n  }, {\n    triggers: ['s'],\n    callback: down\n  }, {\n    triggers: ['a'],\n    callback: left\n  }, {\n    triggers: ['d'],\n    callback: right\n  }];\n  return {\n    handlers: handlers,\n    setSpeed: setSpeed,\n    getSpeed: getSpeed\n  };\n}\n\nvar handleInput = HandleInput(state);\nvar activeInputs = {};\ndocument.addEventListener('keydown', function (e) {\n  return activeInputs[e.key] = true;\n});\ndocument.addEventListener('keyup', function (e) {\n  return activeInputs[e.key] = false;\n});\n\nfunction pollInputs(object, inputEvents, deltaUpdate) {\n  // loop over all the possible inputs that might trigger something\n  for (var i = 0; i < inputEvents.length; i++) {\n    // loop over each key/button press that might   \n    triggerLoop: for (var j = 0; j < inputEvents[j].triggers.length; j++) {\n      if (activeInputs[inputEvents[i].triggers[j]]) {\n        inputEvents[i].callback(object, deltaUpdate); // only want the event that has been triggered to happen once\n\n        break triggerLoop;\n      }\n    }\n  }\n}\n\nMainLoop(function (delta) {\n  ctx.clearRect(0, 0, canvas.width, canvas.height);\n  var deltaUpdate = delta / 1000; // poll inputs and make updates as neccessary\n\n  pollInputs(spriteCrawler, handleInput.handlers, deltaUpdate);\n  crawler.render();\n  spriteCrawler.render();\n});\n\n//# sourceURL=webpack://webpack-html-template/./src/scripts/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/scripts/index.js");
/******/ 	
/******/ })()
;