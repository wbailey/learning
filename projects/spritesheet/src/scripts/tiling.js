class Tile {
  constructor({ i, j }) {
    this.clicked = false
    this.i = i
    this.j = j
    this.x = this.i * baseUnit
    this.y = this.j * baseUnit
    this.width = tileSize
    this.height = tileSize
    console.log(this)
  }
  render() {
    ctx.drawImage(spriteSheet, this.i * tileSize, this.j * tileSize, tileSize, tileSize, this.i * baseUnit, this.j * baseUnit, baseUnit, baseUnit)
    // if(this.clicked) ctx.strokeRect(x, y, tileSize. tileSize)
  }
}
const tiles = [] 

const canvas = document.querySelector('canvas')
canvas.addEventListener('click', (event) => {
  for(let i = 0; i < tiles.length; i++) {
    if(detectHit({ x: event.offsetX, y: event.offsetY, width: 1, height: 1 }, tiles[i])) {
      tiles[i].clicked = true
      console.log(tiles[i])
      break
    }
  }
})
// canvas.setAttribute("height", getComputedStyle(canvas)["width"])
// canvas.setAttribute("width", getComputedStyle(canvas)["width"])
const ctx = canvas.getContext('2d')
const bountifulBitsFull = require('../sprites/bountiful-bits/Full.png')
const springstar = require('../sprites/stringstar-fields/tileset.png')

// find the smaller side
const frame = window.innerHeight < window.innerWidth ? window.innerHeight : window.innerWidth
console.log(frame)
canvas.setAttribute("height", frame)
canvas.setAttribute("width", frame)
// size of imported spritesheet tiles
const tileSize = 16
// size of imported stylesheed
// const imageDimensions = [330, 370]
const imageDimensions = [288, 176]
const [x, y] = imageDimensions 
const cols = x / tileSize
const rows = y / tileSize
// rows/cols of tiles to be shown
const gridSize = 20
// amount to scale display tiles on the grid
const gridScale = 1 / gridSize
// pixel size of a scaled tile 
const baseUnit = canvas.width * gridScale
// imported spritesheet
const spriteSheet = new Image(x, y)
spriteSheet.src = springstar
spriteSheet.onload = () => {
  // for (let i = 0; i <= cols; i++) {
  //   for (let j = 0; j <= rows; j++) {
  //      ctx.drawImage(spriteSheet, i * tileSize, j * tileSize, tileSize, tileSize, i * baseUnit, j * baseUnit, baseUnit, baseUnit)
  //   }
  // } 
  // render()
  for (let i = 0; i <= cols; i++) {
    for (let j = 0; j <= rows; j++) {
      tiles.push(new Tile({ i, j }))
    }
  } 
  render()
}

function detectHit(bodyOne, bodyTwo) {
  if(
    bodyOne.x + bodyOne.width >= bodyTwo.x &&
    bodyOne.x <= bodyTwo.x + bodyTwo.width &&
    bodyOne.y + bodyOne.height >= bodyTwo.y &&
    bodyOne.y <= bodyTwo.y + bodyTwo.height
  ) {
    return true
  }

  return false
}

function render() {
  ctx.clearRect(0, 0, canvas.width, canvas.height)
  for (let i = 0; i < tiles.length; i++) {
    tiles[i].render()
  }
  tiles[0].render()
  requestAnimationFrame(render)
}

// render()
