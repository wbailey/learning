/**
 * MainLoop accepts a callback function and passes in the callback as an argument
 * Mainloop(console.log) -- logs time elapsed from last render
 * @param {*} callback - a callback function to run every loop 
 * @returns request animation frame's return
 */

function MainLoop(callback, fps) {
  // get timestamp when first starting 
  const startTime = new Date()
  // this return function is IIFE to start the loop immediately
  return (function loop (prevTime) {
    // only use starttime first
    prevTime = prevTime ? prevTime : startTime
    // calc th time elapses
    const newTime = new Date()
    const delta = newTime - prevTime
    // run the main loop logic
    callback(delta)
    // recurse this function
    return requestAnimationFrame(() => {
      loop(newTime)
    })
  })()
}

module.exports = MainLoop