const canvas = document.querySelector('canvas')
canvas.setAttribute("height", getComputedStyle(canvas)["width"])
canvas.setAttribute("width", getComputedStyle(canvas)["width"])
const ctx = canvas.getContext('2d')
const MainLoop = require('./MainLoop.js')
// const characterSpriteData = require('../sprites/MYSTIC/characters/characterIdle.json')
// find the smaller side
const spritePath = require('../sprites/MYSTIC/characters/player.png')
const frame = window.innerHeight < window.innerWidth ? window.innerHeight : window.innerWidth
canvas.setAttribute("height", frame)
canvas.setAttribute("width", frame)

// game state
const state = {
  canvas,
  ctx,
  unit: canvas.width * .05,
  initialSpeed: (canvas.width * .05) * 7
}

function Vector({ x, y }) {
  const update = function({ x, y }) {
    this.x = x
    this.y = y
  }

  return {
    x, 
    y,
    update
  }
}

function Crawler({ vector, width, height, color, state }) {
  const { ctx } = state
  const render = function() {
    const { color, width, height, vector } = this
    const { x, y } = vector

    ctx.fillStyle = color
    ctx.fillRect(x, y, width, height)
  }

  return {
    vector,
    width,  
    height,  
    color,
    render
  }
}

function SpriteCrawler({ state, vector, imagePath, width, height }) {
  const { floor } = Math
  const { ctx } = state
  const image = new Image()
  image.src = imagePath

  const render = function() {
    const { image, width, height, vector } = this
    let { x, y } = vector
    x = floor(x)
    y = floor(y)
    ctx.drawImage(
      image,
      0,
      0,
      64,
      64,
      x,
      y,
      64,
      64
    )
  }
  return {
    vector, 
    image, 
    width, 
    height, 
    render
  }
}

const crawler = Crawler({
  vector: Vector({x: 100, y: 100}),
  width: state.unit,
  height: state.unit,
  color: 'green',
  state
})


const spriteCrawler = SpriteCrawler({
  state,
  imagePath: spritePath,
  vector: Vector({x: 20, y: 20}),
  width: 64,
  height: 64,
})

function HandleInput(state) {
  // speed per 1000 ms
  let speed = state.initialSpeed
  console.log(speed)
  const up = function(player, deltaUpdate) {
    player.vector.update({ x: player.vector.x, y: player.vector.y - speed * deltaUpdate })
  }
  
  const down = function(player, deltaUpdate) {
    player.vector.update({ x: player.vector.x, y: player.vector.y + speed * deltaUpdate })
  }
  
  const left = function(player, deltaUpdate) {
    player.vector.update({ x: player.vector.x - speed * deltaUpdate, y: player.vector.y })
    
  }
  
  const right = function(player, deltaUpdate) {
    player.vector.update({ x: player.vector.x + speed * deltaUpdate, y: player.vector.y })
  }

  const getSpeed = function() {
    return speed
  }

  const setSpeed = function(newSpeed) {
    speed = newSpeed
  }
  const handlers = [
    {
      triggers: ['w'],
      callback: up
      
    },
    {
      triggers: ['s'],
      callback: down
      
    },
    {
      triggers: ['a'],
      callback: left
      
    },
    {
      triggers: ['d'],
      callback: right
      
    },
  ]
  return {
   handlers,
   setSpeed,
   getSpeed
  }
}

const handleInput = HandleInput(state)

const activeInputs = {}
document.addEventListener('keydown', e => activeInputs[e.key] = true)
document.addEventListener('keyup', e => activeInputs[e.key] = false) 

function pollInputs(object, inputEvents, deltaUpdate) {
  // loop over all the possible inputs that might trigger something
  for (let i = 0; i < inputEvents.length; i++) {
    // loop over each key/button press that might   
    triggerLoop:
    for (let j = 0; j < inputEvents[j].triggers.length; j++) {
      if (activeInputs[inputEvents[i].triggers[j]]) {
        inputEvents[i].callback(object, deltaUpdate)
        // only want the event that has been triggered to happen once
        break triggerLoop
      }
    }
  }
}

MainLoop(delta => {
  ctx.clearRect(0, 0, canvas.width, canvas.height)
  const deltaUpdate = delta / 1000
  // poll inputs and make updates as neccessary
  pollInputs(spriteCrawler, handleInput.handlers, deltaUpdate)
  crawler.render()
  spriteCrawler.render()
})