export interface Ingredient {
  name: string;
  color: string;
}

export type Ingredients = Array<Ingredient>;
