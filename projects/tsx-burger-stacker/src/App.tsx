import React, { useState } from "react";
import styled from "styled-components"
import { IngredientList, BurgerStack } from "./components";
import "./App.css";
import { Ingredients, Ingredient } from "./types";

const BurgerRow = styled.div`
	display: flex;
	flex-direction: row;
`

interface Props {
  ingredientData: Ingredients;
}

function App(props: Props) {
	const [clickedIngredients, setClickedIngredients] = useState<Array<Ingredient>>([]);
	const handleIngredientClick = (ingredient: Ingredient): void => { 
		setClickedIngredients([ingredient, ...clickedIngredients]);
	}
	const handleBurgerStackClear = (): void => {
		setClickedIngredients([])
	}
	return (
		<BurgerRow>
			<IngredientList 
				ingredientData={props.ingredientData}
				handleIngredientClick={handleIngredientClick}
			/>

			<BurgerStack 
				clickedIngredients={clickedIngredients}
				handleBurgerStackClear={handleBurgerStackClear}
			/>	
		</BurgerRow>	
	);
}

export default App;
