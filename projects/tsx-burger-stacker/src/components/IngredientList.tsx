import React from "react";
import { Ingredient, Ingredients } from "../types";
import styled from "styled-components";

const IngredientButton = styled.button<{ color?: string; }>`
	background-color: ${props => props.color};
	color: ${props => props.color === "#3F250B" ? "#FFFFFF" : "#000000"};
	width: 100%;
`

interface Props {
	ingredientData: Ingredients; 
	handleIngredientClick: (ingredient: Ingredient) => void; 
};

export default function IngrediantList(props: Props) {
	const ingredientButtons = props.ingredientData.map((ingredient, i) => {
		return (
			<div key={`IngrediantButton${i}`}>	
				<IngredientButton
					color={ingredient.color}
					onClick={() => props.handleIngredientClick(ingredient)}
				>
					{ingredient.name}
				</IngredientButton>
			</div>
		)
	});

	return (
		<div>
			{ingredientButtons}	
		</div>
	)
}
