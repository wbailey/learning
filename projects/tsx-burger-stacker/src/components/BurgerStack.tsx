import React from "react";
import { Ingredient } from "../types";
import styled from "styled-components";

const IngredientP = styled.div<{ color?: string; }>`
	background-color: ${props => props.color};
	color: ${props => props.color === "#3F250B" ? "#FFFFFF" : "#000000"};
	width: 100%;
`

interface Props {
	clickedIngredients: Array<Ingredient>;
	handleBurgerStackClear: () => void;
}

export default function BurgerStack(props: Props) {
	const ingredientComponents = props.clickedIngredients.map((ingredient, i) => {
		return (
			<IngredientP	
				key={`clickedIngredients${i}`}
				color={ingredient.color}	
			>
				{ingredient.name}
			</IngredientP>
		)
	})
		
	return (
		<div>
			<button onClick={props.handleBurgerStackClear}>
				I hate Burgers 
			</button>
			{ingredientComponents}
		</div>
	)

}
