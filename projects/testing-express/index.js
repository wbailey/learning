const express = require('express')

const app = express()
const PORT = 8080

app.get('/', (req, res) => {
	res.json({ msg: 'welcome to the API' })
})

app.listen(PORT, err => {
	if (err) {
		console.log(err)
	} else {
		console.log(`listening on port ${PORT}`)
	}
})

module.exports = app
