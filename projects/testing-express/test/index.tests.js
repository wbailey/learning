const expect = require('chai').expect
const request = require('supertest')
const app = require('../index.js')


describe('GET /', () => {
	// tests will be written inside this function
	it('should return a 200', done => {
		request(app)
			.get('/')
			.expect(200, done)
	})
})
