import "dotenv/config";
import "./config/db";
import express from "express";

const app = express();
const PORT = 3000;

app.get("/", (req, res) => {
	res.send("Hello ts Express");
});

app.listen(PORT, () => {
	console.log(`Server is listening on PORT ${PORT}`);
});
