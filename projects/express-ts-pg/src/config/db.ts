import "dotenv/config";
import { Pool, Client } from "pg";

// pools will use environment variables
// for connection information
const pool = new Pool();
console.log(process.env);

(async () => {
	// you can also use async/await
	const res = await pool.query("SELECT 1 + 1");
	// console.log(res)
	await pool.end();
})();

(async () => {
	// clients will also use environment variables
	// for connection information
	const client = new Client();
	await client.connect();

	const res = await client.query("SELECT NOW()");
	await client.end();
})();
