import Canvas from "./components/Canvas";
import './App.css';

const render = (context: CanvasRenderingContext2D, canvas: HTMLCanvasElement | null | undefined): () => void => {
	if (canvas) {	
		context.fillStyle = "purple";
		context.fillRect(0, 0, canvas.width, canvas.height);
	}
	return () => {}
}

function App() {
	return (
		<Canvas render={render}/>
	);
}

export default App
