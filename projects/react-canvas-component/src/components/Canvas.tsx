import { useReducer, useEffect, useRef, useMemo } from "react";

interface Props {
	context?: string;
	width?: number;
	height?: number;
	render(context: CanvasRenderingContext2D, canvas: HTMLCanvasElement | null): () => void;
}



export default function Canvas(props: Props) {
	const canvasRef = useRef<HTMLCanvasElement | null>(null);
	const context = useMemo<CanvasRenderingContext2D | null | undefined>(() => canvasRef.current?.getContext("2d"), [canvasRef]);

	useEffect(() => {
		if (!context) {
			return;
		}

		return props.render(context, canvasRef.current);

	}, [context, props]);
	return (
		<canvas ref={canvasRef} width={props.width || 400} height={props.height || 400}></canvas>
	);
}
