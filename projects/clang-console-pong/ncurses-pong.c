// clang ncurses-pong.c -lncurses 
// #include <stdio.h>
#include <ncurses.h>
#include <unistd.h>

/* ~~~~~~~~~ CONSTANTS ~~~~~~~~~ */

// game loop
#define DELAY 80000

/* ~~~~~~~~~ GAME OBJECTS AND METHODS ~~~~~~~~~ */

struct Ball {
  int velocity_x;
  int velocity_y;
  int x;
  int y;
};

// moves the ball structs, pass the width and height for collision boundaries
void move_ball(struct Ball *ball, int height, int width);

/* ~~~~~~~~~ FUNCTION DEFINITONS ~~~~~~~~~ */

int main(void) 
{
  // game objects
  struct Ball ball;
  ball.velocity_x = 1;
  ball.velocity_y = 1;
  ball.x = 10;
  ball.y = 10;


  /* SETUP WINDOW */

  // size of the board
  int height = 30, width = 60;

  // Global var `stdscr` is created by the call to `initscr()`
  getmaxyx(stdscr, height, width);

  // initialize the window
  initscr();
  // Don't echo keypresses
  noecho();
  // Don't display a cursor
  curs_set(FALSE);

  /* GAME LOOP */

  printf("%d", height);
  sleep(1);
  while(1) {
    // clear the screen
    clear();
    // print characters on the screen
    mvprintw(ball.x, ball.y, "o");
    // update game objects
    move_ball(&ball, height, width);
    // update the screen
    refresh();
    // shorter delay before loop runs again
    usleep(DELAY);
  }

  // pause for one second
  // sleep(1); 
  // restore normal terminal behavior
  endwin();
  return 0;
}

void move_ball(struct Ball *ball, int height, int width) 
{
  if(ball->y >= height - 1 || ball->y <= 1) ball->velocity_y = -ball->velocity_y;
  if(ball->x >= width - 1 || ball->x <= 1) ball->velocity_x = -ball->velocity_x;
  ball->x = ball->x + ball->velocity_x;
  ball->y += ball->velocity_y;
}