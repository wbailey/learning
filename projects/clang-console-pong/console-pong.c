#include <stdio.h>
#include <time.h>

/* ~~~~~~~~~ GAME OBJECTS ~~~~~~~~~ */

struct Ball {
  int velocity_x;
  int velocity_y;
  int x;
  int y;
};



/* ~~~~~~~~~ INTERFACES ~~~~~~~~~ */

// moves the ball structs, pass the width and height for collision boundaries
void move_ball(struct Ball *ball, int height, int width);

// draws the game in the console
void draw(struct Ball ball, int height, int width);

// delay process for cpu clocks equal to click
void delay(int ticks);

/* ~~~~~~~~~ FUNCTION DEFINITONS ~~~~~~~~~ */

int main(void) 
{
  // game objects
  struct Ball ball;
  ball.velocity_x = 1;
  ball.velocity_y = 1;
  ball.x = 10;
  ball.y = 10;

  // size of the board
  int height = 30, width = 60;

  // main game loop
  while(1) {
    delay(50000);
    draw(ball, height, width);
    move_ball(&ball, height, width);
  }
  
  return 0;
}

void move_ball(struct Ball *ball, int height, int width) 
{
  if(ball->y >= height - 1 || ball->y <= 1) ball->velocity_y = -ball->velocity_y;
  if(ball->x >= width - 1 || ball->x <= 1) ball->velocity_x = -ball->velocity_x;
  ball->x = ball->x + ball->velocity_x;
  ball->y += ball->velocity_y;
}

void draw(struct Ball ball, int height, int width) 
{
  // clear the coonsole
  printf("\e[1;1H\e[2J");
  // loop to make board
  int i, j;
  for(i = 0; i <= height; i++) {
    for(j = 0; j <= width; j++) {
      if(i == 0 || i == height) {
        // print the top and bottom of the board
        printf("#");
        continue;
      } else if(j == 0 || j == width) {
        // print the sides
        printf("#");
      } else if(i == ball.y && j == ball.x) {
        // print the ball
        printf("o");
      } else {
        // empty spaces
        printf(" ");
      }
    }
    printf("\n");
  }
}

void delay(int ticks)
{
  // start the clock
  clock_t start_time = clock();
  while(clock() < start_time + ticks);
}