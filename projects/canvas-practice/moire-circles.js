const LOAD_PAGE = document.addEventListener(`DOMContentLoaded`, () => { init(); });
const WIN = window.addEventListener(`resize`, () => { init(); });
const SLIDER_SIZE = document.getElementById(`slider-size`).oninput = function() {sliderInput.lineSize(this.value);}

let canvas,  ctx, canvasWidth, canvasHeight;
let drawWidth = 2;
let sliderInput;

function init() {
  sliderInput =  new SliderInput;
  canvas = document.querySelector("#canvas");
  ctx = canvas.getContext("2d");
  resize();
  draw();
}

function resize() {
  canvasWidth = canvas.width = window.innerWidth;
  canvasHeight = canvas.height = window.innerHeight;
}

class SliderInput {
  lineSize(value){
    drawWidth = value;
    init();
  }
  posX(value){
    x = value;
    init();
  }
  posY(value){
    y = value;
    init();
  }
}

function draw() {
  ctx.lineWidth = drawWidth;
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  let lineCount = (canvasHeight);
  let inc = drawWidth;
  for(let i = 0; i < lineCount; i++){
    ctx.strokeStyle = "black";
    ctx.beginPath();
    //x,y,r,start angle, end angle
    ctx.arc(canvasWidth * .5, canvasHeight * .5, inc, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.strokeStyle = "red";
    ctx.beginPath();
    //x,y,r,start angle, end angle
    ctx.arc(canvasWidth * .3, canvasHeight * .25, inc, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.strokeStyle = "purple";
    ctx.beginPath();
    //x,y,r,start angle, end angle
    ctx.arc(canvasWidth * .1, canvasHeight * .7, inc, 0, 2 * Math.PI);
    ctx.stroke();
    inc += drawWidth * 2;
    //console.log(`line count = ${lineCount} inc = ${inc}`)
  }

  
  
}

// setup();
// draw();