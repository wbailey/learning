const LOAD_PAGE = document.addEventListener(`DOMContentLoaded`, () => { init(); });
const WIN = window.addEventListener(`resize`, () => { init(); });
const SLIDER = document.getElementById(`slider`).oninput = function() {sliderInput(this.value);}

let canvas,  ctx, canvasWidth, canvasHeight;
let drawWidth = 4;

function init() {
  canvas = document.querySelector("#canvas");
  ctx = canvas.getContext("2d");
  resize();
  draw();
}

function resize() {
  canvasWidth = canvas.width = window.innerWidth;
  canvasHeight = canvas.height = window.innerHeight;
}

function sliderInput(value){
  drawWidth = value;
  init();
}

function draw() {
  ctx.lineWidth = drawWidth;
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  ctx.fillStyle = "black";
  let lineCount = (canvasHeight * .5) * .5;
  let inc = 4;
  for(let i = 0; i < lineCount; i++){
    ctx.beginPath();
    ctx.moveTo(0, inc);
    //ctx.lineTo(canvasWidth, inc)
    ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, canvasHeight+5);
    ctx.stroke();
    inc += drawWidth * 2;
    console.log(`line count = ${lineCount} inc = ${inc}`)
  }
  
}

// setup();
// draw();