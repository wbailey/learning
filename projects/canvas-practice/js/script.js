//dom listeners
const LOAD_PAGE = document.addEventListener(`DOMContentLoaded`, () => { init(); });
const WIN = window.addEventListener(`resize`, () => { init(); });
const REFERSH = document.getElementById(`canvas`).addEventListener(`click`, () => { init(); });
const SLIDER_SIZE = document.getElementById(`slider-size`).oninput = function() {sliderInput.lineSize(this.value);}
const TWO_PI = Math.PI * 2;

//canvas variables
let canvas,  ctx, canvasWidth, canvasHeight;
//line width in px
let drawWidth = 2;
let sliderInput;

//called on page load and resize
function init() {
  sliderInput =  new SliderInput;
  canvas = document.querySelector("#canvas");
  ctx = canvas.getContext("2d");
  resize();
  draw();
}

//scales canvas to screen size
function resize() {
  canvasWidth = canvas.width = window.innerWidth;
  canvasHeight = canvas.height = window.innerHeight;
}

//handle input
class SliderInput {
  lineSize(value){
    drawWidth = value;
    let test = scale(value, 1, 32, 0, 1);
    init();
  }
  posX(value){
    x = value;
    init();
  }
  posY(value){
    y = value;
    init();
  }
}

//make random color hex
function randomColorHex(){
  return `#${Math.floor(Math.random()*16777215).toString(16)}`;
}

//remaps value x of known range between xLo and xHi to new range of yLo to yHi
function scale(x, xLo, xHi, yLo, yHi) {
  let percent = (x - xLo) / (xHi - xLo);
  return percent * (yHi - yLo) + yLo;
}

//degrees to radians
function degreesToRadians(angle){
  return (angle / Math.PI) * 180;
}
//degrees to radians
function radiansToDegree(radian){
  return (radian * 180) / Math.PI;
}

function draw() {
  //how many circles to draw
  let lineCount;
  //spacing increment
  let inc = 0;
  //sides of polygons
  let sides = 3;
  //starting radius 
  let radiusX = 200;
  let radiusY = 80;
  //angle of vertices
  let vertAngle = TWO_PI / sides;
  let oneColor = `blue`;
  //random colors and positions of polygpns
  let x = canvasWidth / 2;
  let y = canvasHeight / 2;
  let angle = 1;
  let radians = degreesToRadians(angle);
  let strokeStyle = randomColorHex();
  //let strokeStyle0 = oneColor;
 

  //maximum diameter is based on height or width, which ever is larger
  if(canvasWidth < canvasHeight){
    lineCount = (canvasHeight / drawWidth);
  } else {
    lineCount = (canvasWidth / drawWidth);
  }
  //fill bg
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  //draw polygons
  ctx.lineWidth = drawWidth;
  ctx.strokeStyle = `black`;
  ctx.beginPath();
  ctx.moveTo(x - radiusX * Math.cos(radians), y - radiusY * Math.sin(radians));
  for(let j = 0; j < sides; j++){
    ctx.lineTo(x - radiusX * Math.cos(vertAngle * j + radians), y - radiusY * Math.sin(vertAngle * j + radians));
    angle = vertAngle * j + radians
    console.log(radiansToDegree(angle));
  }
  ctx.closePath();
  ctx.stroke();
     
}
