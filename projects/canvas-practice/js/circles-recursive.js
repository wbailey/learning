//dom listeners
const LOAD_PAGE = document.addEventListener(`DOMContentLoaded`, () => { init(); });
const WIN = window.addEventListener(`resize`, () => { init(); });
const REFERSH = document.getElementById(`canvas`).addEventListener(`click`, () => { init(); });
const SLIDER_SIZE = document.getElementById(`slider-size`).oninput = function() {sliderInput.lineSize(this.value);}

//canvas variables
let canvas, ctx, canvasWidth, canvasHeight;
//line width in px
let drawWidth = 2;
let sliderInput;

// class CirclesRecursive {
//   constructor(circleArgs){
//     this.radius = circleArgs.radius;
//     this.lineWidth = circleArgs.lineWidth;
//     this.recursive = circleArgs.recursive;
//     this.color = circleArgs.color;
//     this.x = circleArgs.x;
//     this.y = circleArgs.y;
//   }
//   drawConcentric(){
//     ctx.strokeStyle = this.color;
//     ctx.beginPath();
//     //x,y,r,start angle, end angle
//     ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
//     ctx.stroke();
//     if(this.radius < canvasHeight * .5){
//       this.radius += this.recursive;
//       return (() => { 
//         this.drawConcentric(); })();
//     } 
//   }
//   drawOnLine(x, radius){
//     ctx.strokeStyle = this.color;
//     ctx.beginPath();
//     //x,y,r,start angle, end angle
//     ctx.arc(x, this.y, radius, 0, 2 * Math.PI);
//     ctx.stroke();
//     if(radius > 2){
//       return ((x, radius) => { 
//         console.log(`called`)
//         radius = radius / 2;
//         this.drawOnLine(x - radius, radius); 
//         this.drawOnLine(x + radius, radius); 
//       })();
//     } 
//   }
// }
/*
let testCircle = {
  radius: 2,
  lineWidth: drawWidth,
  recursive: drawWidth * 2,
  color: randomColorHex(),
  x: canvasWidth * .5,
  y: canvasHeight * .5
}
*/
//concentric
// function drawCircle(circleArgs){
//   ctx.lineWidth = circleArgs.lineWidth;
//   ctx.strokeStyle = circleArgs.color;
//   ctx.beginPath();
//   //x,y,r,start angle, end angle
//   ctx.arc(circleArgs.x, circleArgs.y, circleArgs.radius, 0, 2 * Math.PI);
//   ctx.stroke();
//   if(circleArgs.radius > 2){
//     circleArgs.radius -= drawWidth * 2;
//       return drawCircle(circleArgs)
//   }
// }
// //circles on a line
// function drawCircle(lineWidth, color, x, y, radius){
//   ctx.lineWidth = lineWidth;
//   ctx.strokeStyle = color;
//   ctx.beginPath();
//   //x,y,r,start angle, end angle
//   ctx.arc(x, y, radius, 0, 2 * Math.PI);
//   ctx.stroke();
//   if(radius > 2){
//     return (() => {
//     drawCircle(lineWidth, color, x - radius / 2, y, radius / 2);
//     drawCircle(lineWidth, color, x + radius / 2, y, radius / 2);
//     })();
//   }
// }
//circles on a grid
function drawCircle(lineWidth, color, x, y, radius){
  ctx.lineWidth = lineWidth;
  ctx.strokeStyle = color;
  ctx.beginPath();
  //x,y,r,start angle, end angle
  ctx.arc(x, y, radius, 0, 2 * Math.PI);
  ctx.stroke();
  console.log(radius)
  if(radius > 2){
    return setTimeout(() => {
    // drawCircle(lineWidth, radius % 2 == 0 ? randomRGBA() : color, x - radius / 2, y, radius / 2);
    // drawCircle(lineWidth, Math.ceil(radius) % 2 == 1 ? randomRGBA() : color, x - radius / 2, y, radius / 2);
    drawCircle(lineWidth, randomRGBA(), x - radius / 2, y, radius / 2);
    // drawCircle(lineWidth, color, x - radius / 2, y, radius / 2);
    drawCircle(lineWidth, color, x + radius / 2, y, radius / 2);
    drawCircle(lineWidth, color, x, y - radius / 2, radius / 2);
    drawCircle(lineWidth, color, x, y + radius / 2, radius / 2);
    }, 10)
  }
}


//called on page load and resize
function init() {
  sliderInput =  new SliderInput;
  canvas = document.querySelector("#canvas");
  ctx = canvas.getContext("2d");
  resize();
  
  draw();
}

//scales canvas to screen size
function resize() {
  canvasWidth = canvas.width = window.innerWidth;
  canvasHeight = canvas.height = window.innerHeight;
}

//handle input
class SliderInput {
  lineSize(value){
    drawWidth = value;
    let test = scale(value, 1, 32, 0, 1);
    console.log(test);
    init();
  }
  posX(value){
    x = value;
    init();
  }
  posY(value){
    y = value;
    init();
  }
}

//make random color hex
function randomColorHex(){
  return `#${Math.floor(Math.random()*16777215).toString(16)}`;
}

function randomRGBA(red, blue, green, alpha){
  const r = red || Math.floor(Math.random() * 256),
        g = blue || Math.floor(Math.random() * 256),
        b = green || Math.floor(Math.random() * 256),
        a = alpha || Math.random();
  
      return `rgba(${r},${g},${b},${a})`;
}

//remaps value x of known range between xLo and xHi to new range of yLo to yHi
function scale(x, xLo, xHi, yLo, yHi) {
  const percent = (x - xLo) / (xHi - xLo);
  return percent * (yHi - yLo) + yLo;
}

function draw() {
  //fill bg
  ctx.fillStyle = `black`;
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  //deifne circle arguements
  let testCircle = {
    radius: canvasWidth * 2,
    lineWidth: drawWidth,
    color: randomColorHex(),
    x: canvasWidth * .5,
    y: canvasHeight * .5
  }
  //lineWidth, color, x, y, radius
  drawCircle(2, randomRGBA(), canvasWidth * .5, canvasHeight * .5, canvasHeight * 4);

}

