//dom listeners
const LOAD_PAGE = document.addEventListener(`DOMContentLoaded`, () => { init(); });
const WIN = window.addEventListener(`resize`, () => { init(); });
const REFERSH = document.getElementById(`canvas`).addEventListener(`click`, () => { init(); });
const SLIDER_SIZE = document.getElementById(`slider-size`).oninput = function() {sliderInput.lineSize(this.value);}

//canvas variables
let canvas,  ctx, canvasWidth, canvasHeight;
//line width in px
let drawWidth = 2;
let sliderInput;

//called on page load and resize
function init() {
  sliderInput =  new SliderInput;
  canvas = document.querySelector("#canvas");
  ctx = canvas.getContext("2d");
  resize();
  draw();
}

//scales canvas to screen size
function resize() {
  canvasWidth = canvas.width = window.innerWidth;
  canvasHeight = canvas.height = window.innerHeight;
}

//handle input
class SliderInput {
  lineSize(value){
    drawWidth = value;
    let test = scale(value, 1, 32, 0, 1);
    console.log(test);
    init();
  }
  posX(value){
    x = value;
    init();
  }
  posY(value){
    y = value;
    init();
  }
}

//make random color hex
function randomColorHex(){
  return `#${Math.floor(Math.random()*16777215).toString(16)}`;
}

//remaps value x of known range between xLo and xHi to new range of yLo to yHi
function scale(x, xLo, xHi, yLo, yHi) {
  let percent = (x - xLo) / (xHi - xLo);
  return percent * (yHi - yLo) + yLo;
}

function draw() {
  //how many circles to draw
  let lineCount;
  //spacing increment
  let inc = 0;
  //random colors and positions
  let circle1Style = randomColorHex();
  let circle2Style = randomColorHex();
  let circle3Style = randomColorHex();
  let circle1x = Math.random();
  let circle1y = Math.random();
  let circle2x = Math.random();
  let circle2y = Math.random();
  let circle3x = Math.random();
  let circle3y = Math.random();
  //maximum diameter is based on height or width, which ever is larger
  if(canvasWidth < canvasHeight){
    lineCount = (canvasHeight / drawWidth);
  } else {
    lineCount = (canvasWidth / drawWidth);
  }
  ctx.lineWidth = drawWidth;
  //fill bg
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  //draw circles
  for(let i = 0; i < lineCount; i++){
    ctx.strokeStyle = circle1Style;
    ctx.beginPath();
    //x,y,r,start angle, end angle
    ctx.arc(canvasWidth * circle1x, canvasHeight * circle1y, inc, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.strokeStyle = circle2Style;
    ctx.beginPath();
    //circle 2
    ctx.arc(canvasWidth * circle2x, canvasHeight * circle2y, inc, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.strokeStyle = circle3Style;
    ctx.beginPath();
    //circle 3
    ctx.arc(canvasWidth * circle3x, canvasHeight * circle3y, inc, 0, 2 * Math.PI);
    ctx.stroke();
    //inc to resize future cicles
    inc += drawWidth * 2;
  }
}
