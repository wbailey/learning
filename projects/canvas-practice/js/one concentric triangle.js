//dom listeners
const LOAD_PAGE = document.addEventListener(`DOMContentLoaded`, () => { init(); });
const WIN = window.addEventListener(`resize`, () => { init(); });
const REFERSH = document.getElementById(`canvas`).addEventListener(`click`, () => { init(); });
const SLIDER_SIZE = document.getElementById(`slider-size`).oninput = function() {sliderInput.lineSize(this.value);}
const TWO_PI = Math.PI * 2;

//canvas variables
let canvas,  ctx, canvasWidth, canvasHeight;
//line width in px
let drawWidth = 6;
let sliderInput;

//called on page load and resize
function init() {
  sliderInput =  new SliderInput;
  canvas = document.querySelector("#canvas");
  ctx = canvas.getContext("2d");
  resize();
  draw();
}

//scales canvas to screen size
function resize() {
  canvasWidth = canvas.width = window.innerWidth;
  canvasHeight = canvas.height = window.innerHeight;
}

//handle input
class SliderInput {
  lineSize(value){
    drawWidth = value;
    let test = scale(value, 1, 32, 0, 1);
    init();
  }
  posX(value){
    x = value;
    init();
  }
  posY(value){
    y = value;
    init();
  }
}

//make random color hex
function randomColorHex(){
  return `#${Math.floor(Math.random()*16777215).toString(16)}`;
}

//remaps value x of known range between xLo and xHi to new range of yLo to yHi
function scale(x, xLo, xHi, yLo, yHi) {
  let percent = (x - xLo) / (xHi - xLo);
  return percent * (yHi - yLo) + yLo;
}

//degrees to radians
function degreesToRadians(angle){
  return (angle / Math.PI) * 180;
}

function draw() {
  //how many circles to draw
  let lineCount;
  //spacing increment
  let inc = 0;
  let x = canvasWidth / 2;
  let y = canvasHeight / 2;
  let radius = 80;
  let angle = 240;
  let vertAngle = TWO_PI / 3;
  let radians = degreesToRadians(angle);
  //random colors and positions
  //maximum diameter is based on height or width, which ever is larger
  if(canvasWidth < canvasHeight){
    lineCount = (canvasHeight / drawWidth);
  } else {
    lineCount = (canvasWidth / drawWidth);
  }
  ctx.lineWidth = drawWidth;
  //fill bg
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  //draw circles
  ctx.strokeStyle = 'black';
  for(let i = 0; i < lineCount; i++){
    ctx.moveTo(x - (radius + inc) * Math.cos(radians), y - (radius + inc) * Math.sin(radians));
    for(let j = 0; j < 3; j++){
      ctx.lineTo(x - (radius + inc) * Math.cos(vertAngle * j + radians), y - (radius + inc) * Math.sin(vertAngle * j + radians));
    }
    ctx.closePath();
    ctx.stroke();

    inc += drawWidth * 4;
    //radius += inc;
  }

}
