//dom listeners
const LOAD_PAGE = document.addEventListener(`DOMContentLoaded`, () => { init(); });
const WIN = window.addEventListener(`resize`, () => { init(); });
const REFERSH = document.getElementById(`canvas`).addEventListener(`click`, () => { init(); });
const SLIDER_SIZE = document.getElementById(`slider-size`).oninput = function() {sliderInput.lineSize(this.value);}
const TWO_PI = Math.PI * 2;

//canvas variables
let canvas,  ctx, canvasWidth, canvasHeight;
//line width in px
let drawWidth = 2;
let sliderInput;

//called on page load and resize
function init() {
  sliderInput =  new SliderInput;
  canvas = document.querySelector("#canvas");
  ctx = canvas.getContext("2d");
  resize();
  draw();
}

//scales canvas to screen size
function resize() {
  canvasWidth = canvas.width = window.innerWidth;
  canvasHeight = canvas.height = window.innerHeight;
}

//handle input
class SliderInput {
  lineSize(value){
    drawWidth = value;
    let test = scale(value, 1, 32, 0, 1);
    init();
  }
  posX(value){
    x = value;
    init();
  }
  posY(value){
    y = value;
    init();
  }
}

//make random color hex
function randomColorHex(){
  return `#${Math.floor(Math.random()*16777215).toString(16)}`;
}

//remaps value x of known range between xLo and xHi to new range of yLo to yHi
function scale(x, xLo, xHi, yLo, yHi) {
  let percent = (x - xLo) / (xHi - xLo);
  return percent * (yHi - yLo) + yLo;
}

//degrees to radians
function degreesToRadians(angle){
  return (angle / Math.PI) * 180;
}

function draw() {
  //how many circles to draw
  let lineCount;
  //amount of space between lines
  let lineSpacing;
  //spacing increment
  let inc = 0;
  //sides of polygons
  let sides  = Math.floor(scale(Math.random(), 0, 1, 3, 18));

  //starting radius 
  let radius = 0;
  //angle of vertices
  let vertAngle = TWO_PI / sides;
  let oneColor = `blue`;
  //random colors and positions of polygpns
  let x0 = canvasWidth * Math.random();
  let y0 = canvasHeight * Math.random();
  let angle0 = Math.random() * 360;
  let radians0 = degreesToRadians(angle0);
  let strokeStyle0 = randomColorHex();
  //let strokeStyle0 = oneColor;
  
  let x1 = canvasWidth * Math.random();
  let y1 = canvasHeight * Math.random();
  let angle1 = Math.random() * 360;
  let radians1 = degreesToRadians(angle1);
  let strokeStyle1 = randomColorHex();
  // let strokeStyle1 = oneColor;
  
  let x2 = canvasWidth * Math.random();
  let y2 = canvasHeight * Math.random();
  let angle2 = Math.random() * 360;
  let radians2 = degreesToRadians(angle2);
  let strokeStyle2 = randomColorHex();
  // let strokeStyle2 = oneColor;

  //maximum diameter is based on height or width, which ever is larger
  if(canvasWidth < canvasHeight){
    lineCount = (canvasHeight / drawWidth);
  } else {
    lineCount = (canvasWidth / drawWidth);
  }
  //increase space between lines for less verticies
  if(sides === 3){
    lineSpacing = 4;
  } else if (sides === 4) {
    lineSpacing = 3;
  } else {
    lineSpacing = 2;
  }

  //fill bg
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  //draw polygons
  ctx.lineWidth = drawWidth;
  for(let i = 0; i < lineCount; i++){
    ctx.strokeStyle = strokeStyle0;
    ctx.beginPath();
    ctx.moveTo(x0 - (radius + inc) * Math.cos(radians0), y0 - (radius + inc) * Math.sin(radians0));
    for(let j = 0; j < sides; j++){
      ctx.lineTo(x0 - (radius + inc) * Math.cos(vertAngle * j + radians0), y0 - (radius + inc) * Math.sin(vertAngle * j + radians0));
    }
    ctx.closePath();
    ctx.stroke();

    ctx.strokeStyle = strokeStyle1;
    ctx.beginPath();
    ctx.moveTo(x1 - (radius + inc) * Math.cos(radians1), y1 - (radius + inc) * Math.sin(radians1));
    for(let j = 0; j < sides; j++){
      ctx.lineTo(x1 - (radius + inc) * Math.cos(vertAngle * j + radians1), y1 - (radius + inc) * Math.sin(vertAngle * j + radians1));
    }
    ctx.closePath();
    ctx.stroke();

    ctx.strokeStyle = strokeStyle2;
    ctx.beginPath();
    ctx.moveTo(x2 - (radius + inc) * Math.cos(radians1), y2 - (radius + inc) * Math.sin(radians2));
    for(let j = 0; j < sides; j++){
      ctx.lineTo(x2 - (radius + inc) * Math.cos(vertAngle * j + radians2), y2 - (radius + inc) * Math.sin(vertAngle * j + radians2));
    }
    ctx.closePath();
    ctx.stroke();
    //inc for future polygons
    inc += drawWidth * lineSpacing; //3 three sided polygons look best with drawWidth * 4, four sided = drawWidth * 2.5, else = drawWidth * 2 
  }
}
