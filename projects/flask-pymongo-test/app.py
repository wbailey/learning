from flask import Flask, request, redirect
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/myDatabase"
mongo = PyMongo(app)


@app.route('/')
def hello_world():
  return 'hello from flask 🧉'

@app.route('/drinks', methods=['GET', 'POST'])
def drinks():
  if request.method == 'GET':
    return 'show all drinks'
  elif request.method == 'POST':
    print(request.form)
    return 'make a new drink and redirect'
  else:
    return 'method not allowed'
