# DSA 

## Array

### Static Array

| Opteration | Big-O Time 
| Reading | 0(1)
| Insertion | 0(n)
| Deletion | 0(n)

insterion and deletion are 0(n) becease they require shifting all of the elements in the array as not to leave a hole in it:

Examples: 

[ 0, 1, 2, 3, 4, 5 ]
[ a, b, d, e, f, g ]

Insert c at the 2nd index.

1. shift O(n) where n = length - index: 

[ 0, 1, 2, 3, 4, 5, 6 ]
[ a, b,    d, e, f, g ]

2. insert 0(1): 

[ 0, 1, 2, 3, 4, 5, 6 ]
[ a, b, c, d, e, f, g ]

Deletion:

Delete d from the 3rd index O(1):

[ 0, 1, 2, 3, 4, 5, 6 ]
[ a, b, c,  , e, f, g ]

Shift everything down O(n) where n = length - index:

[ 0, 1, 2, 3, 4, 5 ]
[ a, b, c, e, f, g ]


