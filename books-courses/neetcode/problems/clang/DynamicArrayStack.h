#ifndef _DYNAMIC_ARRAY_STACK_H_
#define _DYNAMIC_ARRAY_STACK_H_

#include "DynamicArray.h"

struct DynamicArrayStack {
	struct DynamicArray *array;
	void (*push) (struct DynamicArrayStack *self, void *value);
	void *(*pop) (struct DynamicArrayStack *self);
};

struct DynamicArrayStack *create_DynamicArrayStack(int size);
void destroy_DynamicArrayStack(struct DynamicArrayStack *self);

#endif // _DYNAMIC_ARRAY_STACK_H_
