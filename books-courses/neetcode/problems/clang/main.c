#include "./main.h"
#include "DynamicArray.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

CREATE_DYNAMIC_ARRAY_PRETTY_PRINT_FUNCTION(int, "%d")

int main(int argc, char *argv[]) {
	struct DynamicArray *my_array = create_DynamicArray(10);

	for (int i = 0; i < 100; i++) {
		int *value = malloc(sizeof(int));
		if (!value) {
			fprintf(stderr, "failed to allocate memory");
			exit(EXIT_FAILURE);
		}
		*value = i;
		my_array->pushback(my_array, value);
	}
	
	int *set_value = malloc(sizeof(int));
	*set_value = -100;
	my_array->set(my_array, 10, set_value);	

	void *popped_back = my_array->popback(my_array);
	printf("popped_back: %d", *(int*) popped_back);
	free(popped_back);

	DynamicArray_pretty_print_int(my_array);
	destroy_DynamicArray(my_array);	

	struct DynamicArrayStack *my_array_stack = create_DynamicArrayStack(10);
	
	for (int i = 0; i < 50; i++) {
		int *value = malloc(sizeof(int));
		*value = i * 10;
		my_array_stack->push(my_array_stack, value);	
	}
	
	printf("popped items ");

	for (int i = 0; i < 30; i++) {
		void *popped_value = my_array_stack->pop(my_array_stack);
		printf("%d ", *(int *) popped_value);
		free(popped_value);
	}
	printf("\n");

	DynamicArray_pretty_print_int(my_array_stack->array);
	

	destroy_DynamicArrayStack(my_array_stack);
}

