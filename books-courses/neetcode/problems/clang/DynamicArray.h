#ifndef _DYNAMIC_ARRAY_H_
#define _DYNAMIC_ARRAY_H_
#include <stdlib.h>

/*
https://neetcode.io/problems/dynamicArray
Design Dynamic Array (Resizable Array)
Design a Dynamic Array (aka a resizable array) class, such as an ArrayList in Java or a vector in C++.

Your DynamicArray class should support the following operations:

DynamicArray(int capacity) will initialize an empty array with a capacity of capacity, where capacity > 0.
int get(int i) will return the element at index i. Assume that index i is valid.
void set(int i, int n) will set the element at index i to n. Assume that index i is valid.
void pushback(int n) will push the element n to the end of the array.
int popback() will pop and return the element at the end of the array. Assume that the array is non-empty.
void resize() will double the capacity of the array.
int getSize() will return the number of elements in the array.
int getCapacity() will return the capacity of the array.
If we call void pushback(int n) but the array is full, we should resize the array first.

Example 1:

Input:
["Array", 1, "getSize", "getCapacity"]

Output:
[null, 0, 1]
Example 2:

Input:
["Array", 1, "pushback", 1, "getCapacity", "pushback", 2, "getCapacity"]

Output:
[null, null, 1, null, 2]
Example 3:

Input:
["Array", 1, "getSize", "getCapacity", "pushback", 1, "getSize", "getCapacity", "pushback", 2, "getSize", "getCapacity", "get", 1, "set", 1, 3, "get", 1, "popback", "getSize", "getCapacity"]

Output:
[null, 0, 1, null, 1, 1, null, 2, 2, 2, null, 3, 3, 1, 2]
Note:

The index i provided to get(int i) and set(int i) is guranteed to be greater than or equal to 0 and less than the number of elements in the array.
*/

struct DynamicArray {
	void **data;
	size_t capacity;
	size_t size;
	size_t min_size;
	void *(*get) (struct DynamicArray *self, int index);
	void (*set) (struct DynamicArray *self, int index, void *value);
	void (*pushback) (struct DynamicArray *self, void *value);
	void *(*popback) (struct DynamicArray *self);
	size_t (*get_size) (struct DynamicArray *self);
};

struct DynamicArray *create_DynamicArray(int min_size);
void destroy_DynamicArray(struct DynamicArray *self);

#define CREATE_DYNAMIC_ARRAY_PRETTY_PRINT_FUNCTION(type, format) \
	void DynamicArray_pretty_print_##type(struct DynamicArray *self) { \
		printf("(%p) DynamicArray->size: %d, DynamicArray->capacity: %d, DynamicArray->min_size: %d, [ ", self, (int) self->size, (int) self->capacity, (int) self->min_size); \
		for (int i = 0; i < self->size; i++) { \
			printf(format, *(type*) self->get(self, i)); \
			if (i < self->size - 1) { \
				printf(", "); \
			} \
		} \
		printf(" ]\n"); \
	}

#endif //_DYNAMIC_ARRAY_H_
