#include "DynamicArrayStack.h"
#include "DynamicArray.h"

#include <stdlib.h>

static void push (struct DynamicArrayStack *self, void *value);
static void *pop (struct DynamicArrayStack *self);

struct DynamicArrayStack *create_DynamicArrayStack(int size) {
	struct DynamicArrayStack *self = malloc(sizeof(struct DynamicArrayStack));
	self->array = create_DynamicArray(size);
	self->push = &push;
	self->pop = &pop;
	return self;
}

void destroy_DynamicArrayStack(struct DynamicArrayStack *self) {
	destroy_DynamicArray(self->array);
	free(self);
}

static void push(struct DynamicArrayStack *self, void *value) {
	self->array->pushback(self->array, value);
}

static void *pop(struct DynamicArrayStack *self) {
	return self->array->popback(self->array); // callers needs to free
}
