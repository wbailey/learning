#include "DynamicArray.h"
#include <stdlib.h>

// static functions
static size_t get_capacity(struct DynamicArray *self);
static int resize(struct DynamicArray *self);

// instance methods
static void *get (struct DynamicArray *self, int index);
static void set (struct DynamicArray *self, int index, void *value);
static void pushback (struct DynamicArray *self, void *value);
static void *popback (struct DynamicArray *self);
static size_t get_size (struct DynamicArray *self);

// constructor/destructor
struct DynamicArray *create_DynamicArray(int size) {
	struct DynamicArray *new_array = malloc(sizeof(struct DynamicArray));
	if (!new_array) {
		return NULL;
	}
	new_array->data = malloc(sizeof(void *) * size);
	if (!new_array->data) {
		free(new_array);
		return NULL;
	}
	new_array->min_size = size;
	new_array->capacity = size;
	new_array->size = 0;
	new_array->get = &get;
	new_array->set = &set;
	new_array->pushback = &pushback;
	new_array->popback = &popback;
	new_array->get_size = &get_size;
	return new_array;

}
void destroy_DynamicArray(struct DynamicArray *self) {
	for (int i = 0; i < self->get_size(self); i++) {
		free(self->data[i]);
	}
	free(self->data);
	free(self);
}

// private functions 
static size_t get_capacity(struct DynamicArray *self) {
	return self->capacity;
}
static int resize(struct DynamicArray *self) {

	if (self->get_size(self) >= get_capacity(self)) {
		size_t new_capacity = get_capacity(self) * 2;
		void *new_data = realloc(self->data, sizeof(void *) * new_capacity);
		if (!new_data) {
			return 0;
		}
		self->data = new_data;
		self->capacity = new_capacity;
	} else if (self->get_size(self) <= get_capacity(self) / 2 && get_capacity(self) > self->min_size) {
		size_t new_capacity = get_capacity(self) / 2;
		void *new_data = realloc(self->data, sizeof(void *) * new_capacity);
		if (!new_data) {
			return 0;
		}
		self->data = new_data;
		self->capacity = new_capacity;
	}

	return 1;
}

// instance methods
static void *get(struct DynamicArray *self, int index) {
	if (index < 0 || index > self->get_size(self) - 1) {
		return NULL;
	}
	return self->data[index];
}

static void set(struct DynamicArray *self, int index, void *value) {	
	if (index < 0 || index > self->get_size(self) - 1) {
		return;
	}
	free(self->data[index]);
	self->data[index] = value;
}

static void pushback(struct DynamicArray *self, void *value) {
	self->data[self->get_size(self)] = value;
	self->size++;
	resize(self);
}

static void *popback(struct DynamicArray *self) {
	self->size--;
	return self->data[self->get_size(self)]; // caller is responsible to free
}

static size_t get_size(struct DynamicArray *self) {
	return self->size;
}
