from DynamicArray import DynamicArray


class Stack:
    def __init__(self):
        self.data = DynamicArray(10)

    def __str__(self):
        return f"({self.__repr__()}) :: data: {self.data}"

    def push(self, n):
        self.data.pushback(n)

    def pop(self):
        return self.data.popback()


if __name__ == "__main__":
    stack = Stack()
    stack.push("a")
    stack.push("b")
    stack.push("c")
    stack.push("d")
    print(stack)
    print(stack.pop())
    print(stack)
