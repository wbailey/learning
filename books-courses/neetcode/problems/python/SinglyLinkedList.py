"""
Design Singly Linked List

Design a Singly Linked List class.

Your LinkedList class should support the following operations:

    LinkedList() will initialize an empty linked list.
    int get(int i) will return the value of the ith node (0-indexed). If the index is out of bounds, return -1.
    void insertHead(int val) will insert a node with val at the head of the list.
    void insertTail(int val) will insert a node with val at the tail of the list.
    int remove(int i) will remove the ith node (0-indexed). If the index is out of bounds, return false, otherwise return true.
    int[] getValues() return an array of all the values in the linked list, ordered from head to tail.

Example 1:

Input: 
["insertHead", 1, "insertTail", 2, "insertHead", 0, "remove", 1, "getValues"]

Output:
[null, null, null, true, [0, 2]]

Example 2:

Input:
["insertHead", 1, "insertHead", 2, "get", 5]

Output:
[null, null, -1]

Note:

    The index int i provided to get(int i) and remove(int i) is guranteed to be greater than or equal to 0.


"""


class SinglyLinkedListNode:
    def __init__(self, val: any = None, next: any = None):
        self.val = val
        self.next = next

    def __str__(self):
        return f"Node ({self.__repr__()}): {self.val}"


class SinglyLinkedList:

    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def __str__(self):
        string = f"SinglyLinkedList ({self.__repr__()}), size: {len(self)}: ["
        current_node = self.head
        while current_node:
            string = f"{string} {current_node.val}"
            current_node = current_node.next
        return f"{string} ]"
    
    def __len__(self):
        return self.size

    def get(self, index: int) -> int:
        if index < 0:
            return -1

        i = 0
        current_node = self.head
        while current_node:
            if i == index:
                return current_node.val
            current_node = current_node.next

        return -1

    def insertHead(self, val: any) -> None:
        if not self.head:
            self.head = SinglyLinkedListNode(val)
            self.tail = self.head
            return

        self.head = SinglyLinkedListNode(val, self.head)

    def insertTail(self, val: any) -> None:
        if not self.tail:
            self.tail = SinglyLinkedListNode(val)
            self.head = self.tail
            self.size += 1
            return

        new_node = SinglyLinkedListNode(val)
        self.tail.next = new_node
        self.tail = new_node
        self.size += 1

    def remove(self, index: any) -> bool:
        if index >= len(self) or index < 0:
            return False

        i = 0
        current_node = self.head
        while current_node:
            if i == index - 1:
                current_node.next = current_node.next.next
                return True
            i += 1
            current_node = current_node.next
        
        return False

    def getValues(self) -> list[any]:
        li = []
        current_node = self.head
        while current_node:
            li.append(current_node.val)
            current_node = current_node.next
        return li


if __name__ == "__main__":
    li = SinglyLinkedList()
    li.insertTail(0)
    li.insertTail(1)
    li.insertTail(2)
    li.insertTail(3)
    li.insertTail(4)
    li.insertTail(5)
    li.insertTail("hello")
    li.remove(3)
    print(li.getValues())
    print(li)
