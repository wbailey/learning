#include <stdio.h>

int main (int argc, char *argv[])
{
  int areas[] = { 10, 12, 13, 14, 20 };
  char name[] = "Wes";
  char full_name[] = { 'W', 'e', 's', ' ', 'D', '.', ' ', 'B', 'a', 'i', 'l', 'e', 'y' };

  // WARNING: on some systems you may have to change the
  // %ld in the code %u since it will use unsigned ints
  printf ("the size of an int: %ld\n", sizeof(int));
  printf ("the size of areas (int[]): %ld\n", sizeof(areas));
  printf ("the number of ints in areas: %ld\n", sizeof(areas) / sizeof(int));
  printf ("the size of first area is %d and the size of the seocnd area %d\n", areas[0], areas[1]);

  printf ("the size of the a char is %ld\n", sizeof(char));
  printf ("the size of name (name[]) %ld\n", sizeof(name));
  printf ("The number of chars %ld\n", sizeof(name) / sizeof(char));

  printf ("the size of name (full_name[]) %ld\n", sizeof(full_name));
  printf ("The number of chars in full name %ld\n", sizeof(full_name) / sizeof(char));

  printf ("name=\"%s\" and full_name=\"%s\"\n", name, full_name);

  return 0;
}
