#include <stdio.h>

/* this is a comment */
int main (int argc,  char *argv[])
{
  // argc -- argument count
  // argv argument vectors
  int distance = 100;
  int speed = 60;
  // this is also a comment
  printf ("You are %d miles away, traveling at %dmph. \n", distance, speed);
  printf ("speed first %2$dmph now distance %1$d and speed again %2$d \n", distance, speed);

  return 0;
}
