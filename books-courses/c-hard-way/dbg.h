#ifndef _DBG_H_
#define _DBG_H_

#include <stdio.h>
#include <errno.h>
#include <string.h>

#define DBG_H_RED "\033[31m"
#define DBG_H_GREEN "\033[32m"
#define DBG_H_YELLOW "\033[33m"
#define DBG_H_BLUE "\033[34m"

#define DBG_H_RESET "\033[0m"

#ifdef NDEBUG
#define debug(Message, ...)
#else
#define debug(Message, ...) fprintf(stderr, \
		DBG_H_BLUE  "[DEBUG]" DBG_H_RESET "(%s:%d: {%s}) " Message "\n", \
        __FILE__, __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__)
#endif /* NDEBUG */

#define clean_errno() (errno == 0 ? "None" : strerror(errno))

#define log_err(Message, ...) fprintf(stderr,\
       DBG_H_RED "[ERROR]" DBG_H_RESET "(%s:%d: {%s}: errno: %s) " Message "\n", \
		__FILE__, __LINE__, __PRETTY_FUNCTION__, \
        clean_errno(), ##__VA_ARGS__)

#define log_warn(Message, ...) fprintf(stderr,\
        DBG_H_YELLOW "[WARN]" DBG_H_RESET "(%s:%d: {%s}: errno: %s) " Message "\n",\
        __FILE__, __LINE__, __PRETTY_FUNCTION__, \
		clean_errno(), ##__VA_ARGS__)

#define log_info(Message, ...) fprintf(stderr, \
	 	  DBG_H_BLUE "[INFO]" DBG_H_RESET  "(%s:%d: {%s}) " Message "\n",\
        __FILE__, __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__)

#define check(Assert, Message, ...) if(!(Assert)) {\
    log_err(Message, ##__VA_ARGS__); errno=0; goto error; }

#define sentinel(Message, ...)  { log_err(Message, ##__VA_ARGS__);\
    errno=0; goto error; }

#define check_mem(Assert) check((Assert), "Out of memory.")

#define check_debug(Assert, Message, ...) if(!(Assert)) { \
	debug(Message, ##__VA_ARGS__); errno=0; goto error; }

#endif /* _DBG_H_ */
