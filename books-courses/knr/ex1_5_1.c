/* 
   exercise 1.5.1, page 16, read input stream and output
   this program breaks the terminal
*/
#include <stdio.h>

int main(int argc, char *argv[])
{
	int c;

	c = getchar();
	while (c != EOF) {
		putchar(c);
	}

	return 0;
}

