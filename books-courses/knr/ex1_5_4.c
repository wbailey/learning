/* 
	exercise 1.5.4, word counting, page 20 
	a very simple implementation of the wc command
	count lines, words and characters on input
*/
#include <stdio.h>

#define IN 1		/* inside a word */
#define OUT 0		/* outside a word */

int main (int argc, char *arv[])
{
	int c, nl, nw, nc, state;

	state = OUT;
	nl = nw = nc = 0;
	while ((c = getchar()) != EOF) {
		++nc;
		if (c == '\n')
			++nl;
		if (c == ' ' || c == '\n' || c == '\t')
			state = OUT;
		else if (state == OUT) {
			state = IN;
			++nw;
		}
	}
	printf("lines: %d, words: %d, characters: %d\n", nl, nw, nc);
}
