
# recursive complement machine
def compliment_machine(count):
  # first invocation is determined by there being no count
  if not count: count = 1
  # introduce itself and ask for the user's name
  print('Welcome to the python complement machine! UwU')
  print('Enter your name to recieve an unsolicated compliment')
  prompt = '>>'
  name = input(prompt)

  # give the user a compliment with an f string!
  print(f'{name}! That is such a beautiful name!')

  # python if statements
  plural = 'complements'
  if count == 1: plural = plural[0:-1]

  print(f'I have given {count} {plural} today!')

  if count < 4:
    print('I sure would like to give some more complements!')
  elif count < 8:
    print('I love giving complements! Love it so much!')
  # base case greater than 8 compliments
  else:
    print('I have given enough compliments! Time for A Break!')
    print('Goodbye! 👋')
    return

  # recursively invoke complement_machine!!!!!!!
  return compliment_machine(count + 1)


# compliment_machine(1)

# from sys import argv

# script, first, second, third = argv

