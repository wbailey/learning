# Cloud Computing Essentials

* IaaS -- infrastructure as a service (runs hardware and
you run os/middleware and middleware
* PaaS -- platform as a service everything except the application to deplot
* SaaS -- software running on the cloud that is the application that has been deployed
* Containers - runtime where states the requirements to run the application are specified and ran independalty of the OS 
	* elasticity -- (scale up and down) CPU, Disk, Memory are all scaled based on needs at a given time
	* Scalabilty -- load balancers scale servers in and out at work
	* cost -- pay for what you use (instead of maintaining the peak load needs)
* microservices -- breaking all parts of an solution (app) into tiny parts
* serverless -- not actually deploying on specfic hardware but using a constellation of SaaS and messages to each service
* modern/cloud native applications -- containerize applications ready to cloud deploy
* express route -- fiber optic connection to cloud infrastructure to avoid public internet

