# Intro to cloud computing

* hypervisor layer?

# Day 1

## 12 factor app design

https://12factor.net/

* avoid software rot
* control the dynamics of organic growth
* improve collaboration


### Codebase

* each app needs to be a single codebase
	* multiple codebases is defined as a distrubuted system

### dependancies

* app must declare all dependancies (no bash unless its part of a VM/comtainer)
	* never relies on system dependancies 

### Config 

* needs to be in the envirment
* easy to change between deploys
* managed independantly from each deploy

### Backing Services

* services relied upon are abstracted out of the code into config
* (REACT_APP_SERVEF_URL), (POSTGRES URL)
* can be swapped out

### Build Release Run

* three stages are separate
* build -- complie
* relaese -- deploy
* run -- runs in execution enviroment

### Processes 

* stateless and share nothing
* all data needs to be stored in a backing service
* no cache

### PORT binding

* apps are self contained and don't require

### Concurrency

* scale out -- dont scale up
* make copies of app to handle load

### Disposablilty

* shutdown gracefully when given a sigterm and needs to spin down
* can recoverfrom kernal panic

### Dev/Prod Parity

* dev, staging and prod should all be similar as possible

### Logs 

* goes ot stdout for consistency

### Admin Process

* runable command like migraiton

Day 2

date -- 2/26 - 3/8, 4/1 - 12, 4/29 - 5/10

confirmation nuo: 42612SE024011
