from typing import Any

class HashTable:
    """
        class HashTable:\n
        implements a key/value pair store data structure\n
            hash(key) -> int:
                returns a hash for a key

            insert(key, key, value) -> None:
                inserts a key/value pair into the table

            remove(key) -> None:
                removes a key from the table

            get(key) -> any:
                looks up a key and returns a value
    """
    def __init__(self):
        self.table = [[] for _ in range(256)]

    def hash_function(self, key: str) -> int:
        pass
