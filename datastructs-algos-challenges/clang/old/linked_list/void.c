#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

struct list {
	struct node *head;
	struct node *tail;
	int length;
	void (*append) (struct list *self, void *data);
	void (*prepend) (struct list *self, void *data);
	void (*for_each) (struct list *self, void (*callback)(struct node*));
	/*
	   (python)
	   clear
	   count
	   (js)
	   reverse
	   concat
	   sort
	   map
	   filter
	   reduce
	   every
	   some
	   index_of
	   last_index_of
	*/
};

struct node {
	void *data;
	struct node *next;
};

/* constructor/destructor */
struct list *list_create(void);
void list_destroy(struct list *li);

struct node *node_create(void *data);
void node_destroy(struct node *no);

/* instance methods */
void append(struct list *self, void *data);
void prepend(struct list *self, void *data);
void for_each(struct list *self, void (*callback)(struct node*));

void dummy (struct node *no);

int main (int argc, char *argv[])
{
	struct list *my_list = list_create();

	printf("appending 10\n");
	int test_num = 10;
	my_list->append(my_list, &test_num);
	printf("head should be 10: %d\n", *(int*) my_list->head->data);	
	printf("tail should be 10: %d\n", *(int*) my_list->tail->data);	
	
	printf("appending 13\n");
	int other_num = 13;
	my_list->append(my_list, &other_num);
	printf("head should be 10: %d\n", *(int*) my_list->head->data);
	printf("tail should be 13: %d\n", *(int*) my_list->tail->data);

	printf("prepending 7\n");
	int prepend_num = 7;
	my_list->prepend(my_list, &prepend_num);	
	printf("head should be 7: %d\n", *(int*) my_list->head->data);
	printf("tail should be 13: %d\n", *(int*) my_list->tail->data);

	list_destroy(my_list);

	return 0;
}

/* constructor/destructor */

struct list *list_create(void)
{
	struct list *new_list = malloc(sizeof(struct list));
	new_list->head = NULL;
	new_list->tail = NULL;
	new_list->append = &append;
	new_list->prepend = &prepend;
	new_list->for_each = &for_each;
	return new_list;
}

void list_destroy(struct list *self)
{
	struct node *current_node = self->head;
	while (current_node) {
		struct node *next_node = current_node->next;
		printf("about to free %d\n", *(int*) current_node->data);
		free(current_node);
		current_node = next_node;
	}
	free(self);
}

struct node *node_create(void *data)
{
	struct node *new_node = malloc(sizeof(struct node));

	new_node->data = data;
	new_node->next = NULL;
	
	return new_node;
}

void node_destroy(struct node *no) 
{
	free(no);
}

/* instance methods */
void append(struct list *self, void *data)
{
	struct node *new_node = node_create(data);
	/* if this is the first value in the list */
	if (self->length == 0) {
		self->head = new_node;
		self->tail = new_node;
	} else {
		self->tail->next = new_node;
		self->tail = new_node;
	}
	self->length++;
	return;
}

void prepend(struct list *self, void *data)
{
	struct node *new_node = node_create(data);
	if (self->length == 0) {
		self->head = new_node;
		self->tail = new_node;
	} else {
		new_node->next = self->head;
		self->head = new_node;
	}
	self->length++;
	return;
}

void for_each(struct list *self, void (*callback)(struct node*))
{
	for (struct node *current_node = self->head; 
			current_node != NULL; 
			current_node = current_node->next) {
		callback(current_node);
		// printf("hello: %d\n", *(int*) current_node->data);
	}
}

void dummy (struct node *no) {
	printf("hello: %d\n", *(int*) no->data);
}
